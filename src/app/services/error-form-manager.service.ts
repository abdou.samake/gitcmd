import {Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ErrorsModelEnum} from '../models/errors.model';

@Injectable({
  providedIn: 'root'
})
export class ErrorFormManagerService {

  getErrorsMap(formToCheck: FormGroup): Map<string, string> {
    const result = new Map();
    Object.keys(formToCheck.controls)
      .forEach((key: string) => formToCheck.get(key).errors ? result.set(key, ErrorsModelEnum[key]) : null);
    return result;
  }

  getErrorsToString(formToCheck: FormGroup): string {
    return Array.from(this.getErrorsMap(formToCheck).values()).reduce((acc, key) => acc + (`✗ ${key} \n`), '');
  }
}
