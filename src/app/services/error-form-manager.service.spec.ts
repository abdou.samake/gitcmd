import { TestBed } from '@angular/core/testing';

import { ErrorFormManagerService } from './error-form-manager.service';

describe('ErrorFormManagerService', () => {
  let service: ErrorFormManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorFormManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
