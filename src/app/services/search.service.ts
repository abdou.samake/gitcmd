import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserModel, UserRolesEnum } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class SearchService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }


  searchUser$(nameToSearch: string, role?: UserRolesEnum): Observable<UserModel[]> {

    const strSearch = nameToSearch;
    const strLength = strSearch.length;
    const strFrontCode = strSearch.slice(0, strLength - 1);
    const strEndCode = strSearch.slice(strLength - 1, strSearch.length);
    const startCode = strSearch;

    const endCode = strFrontCode + String.fromCharCode(strEndCode.charCodeAt(0) + 1);
    if (role) {
      return this.afs.collection<UserModel>('users', ref => ref
        .where('lastName', '>=', startCode)
        .where('lastName', '<', endCode)
        .where(role, '==', true),
      )
        .valueChanges();
    } else {
      return this.afs.collection<UserModel>('users', ref => ref
        .where('lastName', '>=', startCode)
        .where('lastName', '<', endCode),
      )
        .valueChanges();
    }


  }
}
