import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { auth } from 'firebase/app';
import 'firebase/auth';
import { from, Observable } from 'rxjs';
import { filter, map, switchMap, tap } from 'rxjs/operators';
import { UserService } from './user.service';
import UserCredential = firebase.auth.UserCredential;

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(
    private afs: AngularFirestore,
    private afa: AngularFireAuth,
    private router: Router,
    private zone: NgZone,
    private userService: UserService,
  ) {
  }

  connectWithEmailAndPassword$(email: string, password: string): Observable<UserCredential> {
    return from(this.afa.signInWithEmailAndPassword(email, password));
  }

  signInWithGoogle$() {
    return from(this.afa.signInWithPopup(new auth.GoogleAuthProvider()));
  }

  createAccountWithEmailAndPassword$(email: string, password: string): Observable<UserCredential> {
    return from(this.afa.createUserWithEmailAndPassword(email, password));
  }

  signOut$(): Observable<void> {
    return from(this.afa.signOut());
  }

  resetEmail$(email: string): Observable<void> {
    return from(this.afa.sendPasswordResetEmail(email));
  }

  handleAuthState() {
    this.afa.user
      .pipe(
        tap((firebaseUser: firebase.User | null) => {
          if (!firebaseUser) {
            return this.router.navigate(['/auth']);
          }
        }),
        filter((firebaseUser: firebase.User | null) => !!firebaseUser),
        switchMap((firebaseUser: firebase.User | null) => this.userService.getMyData$(firebaseUser.uid)
          .pipe(
            map(() => firebaseUser),
          )),
        switchMap((firebaseUser: firebase.User) => from(firebaseUser.getIdTokenResult(true))),

      )
      .subscribe();
  }

  changePassword(newPassword: string) {
    return from(this.afa.currentUser)
      .pipe(
        switchMap((firebaseUser: firebase.User | null) => firebaseUser.updatePassword(newPassword)),
      );
  }

}
