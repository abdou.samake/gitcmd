import { Injectable } from '@angular/core';
import { ClaimDatabaseType, ClaimsEnum, ClaimsRoleNameInDB, ClaimType } from '../models/claims.model';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {

  constructor() {
  }

  getClaimBooleanName(type: ClaimsEnum): ClaimType {
    return Object.values(ClaimsEnum).find(claim => type === claim);
  }

  getClaimDatabaseName(type: ClaimType): ClaimDatabaseType {
    return ClaimsRoleNameInDB[type];
  }

  getClaimBoolName(type: ClaimType): ClaimsEnum {
    return ClaimsEnum[type];
  }

}
