import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NzModalRef, NzModalService, OnClickCallback } from 'ng-zorro-antd';
import { ErrorFormManagerService } from './error-form-manager.service';

@Injectable({
  providedIn: 'root',
})
export class ModalsManagerService {

  constructor(
    private modal: NzModalService,
    private errorFormManagerService: ErrorFormManagerService,
  ) {
  }

  info(title: string, message: string): NzModalRef<unknown> {
    return this.modal.info({
      nzTitle: title,
      nzContent: message,
      nzClosable: true,
      nzOkText: `Ok`,
      nzStyle: {'white-space': 'pre-line'},
    });
  }

  errorOnForm(form: FormGroup): NzModalRef<unknown> {
    return this.modal.error(
      {
        nzTitle: 'Il y a des erreurs sur le formulaire',
        nzContent: this.errorFormManagerService.getErrorsToString(form),
        nzClosable: true,
        nzOkText: `J'ai compris`,
        nzStyle: {'white-space': 'pre-line'},
      },
    );
  }

  error(message: string, onOk?: OnClickCallback<void>): NzModalRef<unknown> {
    const modal = this.modal.error({
      nzTitle: 'Une erreur est survenue',
      nzContent: message,
      nzClosable: true,
      nzOkText: `J'ai compris`,
      nzOnOk: () => {
        if (onOk) {
          onOk();
        }
        modal.close();
      },
    });
    return modal;
  }
}
