import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { combineLatest, from, Observable, of } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';
import { ClaimsEnum } from '../models/claims.model';
import { EventModel, FullEventModel } from '../models/eventModel';
import { MonitoringModel } from '../models/monitoring.model';
import { UserModel } from '../models/user.model';
import { WorkshopModel } from '../models/workshop.model';
import FieldValue = firebase.firestore.FieldValue;

@Injectable({
  providedIn: 'root',
})
export class EventService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }

  getFullEventById$(uid: string): Observable<FullEventModel> {
    return this.afs.collection('events').doc<EventModel>(uid).valueChanges()
      .pipe(
        switchMap((event: EventModel) => this.afs
          .collection('workshops')
          .doc<WorkshopModel>(event.workshopUid)
          .valueChanges()
          .pipe(
            map((workshop) => ({workshop, event})),
          ),
        ),
        switchMap((fullEvent: FullEventModel) => this.afs
          .collection('organizations')
          .doc(fullEvent.workshop.organizationUid).valueChanges()
          .pipe(map(organization => ({organization, ...fullEvent}))),
        ),
      );
  }

  saveEvent$(event: EventModel): Observable<void> {
    return from(this.afs.collection('events').doc(event.uid).set(event, {merge: true}));
  }

  addUserToAnEventByType$(event: EventModel, user: UserModel, type: ClaimsEnum) {
    return combineLatest([
      from(this.afs
        .collection('eventsPrivates').doc(event.uid)
        .set({
          updateDate: FieldValue.serverTimestamp(),
        }, {merge: true}),
      ),
      from(this.afs
        .collection('eventsPrivates').doc(event.uid)
        .collection(type).doc(user.uid)
        .set({
          userUid: user.uid,
          userRef: user?.ref ?? this.afs.collection('users').doc(user.uid).ref,
          creationDate: FieldValue.serverTimestamp(),
        }, {merge: true}),
      ),
      this._updateReservedMembers$(event),
      from(this.afs
        .collection('users').doc(user.uid)
        .collection('events').doc(event.uid)
        .set({
          [type]: true,
          userUid: user.uid,
          userRef: user?.ref ?? this.afs.collection('users').doc(user.uid).ref,
          creationDate: FieldValue.serverTimestamp(),
          eventRef: event?.ref ?? this.afs.collection('events').doc(event.uid).ref,
          eventUid: event.uid,
        }, {merge: true})),
    ]);
  }

  removeUserFromAnEventByType$(event: EventModel, user: UserModel, type: ClaimsEnum) {
    return combineLatest([
      from(this.afs
        .collection('eventsPrivates').doc(event.uid)
        .set({
          updateDate: FieldValue.serverTimestamp(),
        }, {merge: true}),
      ),
      from(this.afs
        .collection('eventsPrivates').doc(event.uid)
        .collection(type).doc(user.uid)
        .delete(),
      ),
      this._updateReservedMembers$(event),
      from(this.afs
        .collection('users').doc(user.uid)
        .collection('events').doc(event.uid)
        .delete()),
    ]);
  }

  getAllUsersOfAnEventByType$(event: EventModel, type: ClaimsEnum): Observable<any> {
    return this.afs
      .collection('eventsPrivates').doc(event.uid)
      .collection<{ userUid: string }>(type || ClaimsEnum.isUser).valueChanges()
      .pipe(
        switchMap((users) => users.length > 0
          ? combineLatest(users.map((user) =>
            this.afs
              .collection('users')
              .doc<MonitoringModel>(user.userUid)
              .valueChanges().pipe(map(userData => {
              userData.event = event;
              return userData;
            }))),
          )
          : of([])),
      );
  }

  private _updateReservedMembers$(event: EventModel) {
    return this.afs
      .collection('eventsPrivates')
      .doc(event.uid)
      .collection<any>('isUser')
      .valueChanges()
      .pipe(
        switchMap((list: any[]) => from(this.afs
          .collection('events')
          .doc<EventModel>(event.uid)
          .set({reserved: list.length as number || 0}, {merge: true}))),
      );
  }

}
