import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { DocumentReference } from '@angular/fire/firestore/interfaces';
import * as firebase from 'firebase/app';
import { combineLatest, from, Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { ClaimType } from '../models/claims.model';
import { OrganizationModel } from '../models/organization.model';
import { UserModel } from '../models/user.model';
import { UtilsService } from './utils.service';
import FieldValue = firebase.firestore.FieldValue;

@Injectable({
  providedIn: 'root',
})
export class OrganizationService {

  organizationsRef: AngularFirestoreCollection<OrganizationModel> = this.afs.collection<OrganizationModel>('organizations');
  organizationsPrivatesRef: AngularFirestoreCollection<OrganizationModel> = this.afs.collection('organizationsPrivates');
  usersRef: AngularFirestoreCollection<UserModel> = this.afs.collection('users');

  constructor(
    private afs: AngularFirestore,
    private utils: UtilsService,
  ) {
  }

  createOrganization$(organization: OrganizationModel): Observable<OrganizationModel> {
    const {name, autoValidation}: OrganizationModel = organization;
    return from(this.organizationsRef.add({name}))
      .pipe(
        switchMap((result: DocumentReference) => combineLatest([
            from(this.organizationsRef.doc<OrganizationModel>(result.id).set({
              uid: result.id,
              ref: result,
              creationDate: FieldValue.serverTimestamp(),
            }, {merge: true})),
            from(this.organizationsPrivatesRef.doc(result.id).set({autoValidation}, {merge: true})),
          ])
            .pipe(
              map(() => ({...organization, uid: result.id})),
            ),
        ),
      );
  }

  getOrganisationById$(uid: string): Observable<OrganizationModel> {
    return combineLatest([
      this.afs.collection('organizations').doc<OrganizationModel>(uid).valueChanges(),
      this.afs.collection('organizationsPrivates').doc<OrganizationModel>(uid).valueChanges(),
    ]).pipe(
      map(([data1, data2]) => ({...data1, ...data2})),
    );
  }

  removeUserFromOrganization$(user: UserModel, organization: OrganizationModel, type: ClaimType): Observable<[void, void]> {
    return combineLatest([
      from(this.organizationsPrivatesRef
        .doc(organization.uid)
        .collection(this.utils.getClaimDatabaseName(type))
        .doc(user.uid).delete()),
      from(this.usersRef
        .doc(user.uid)
        .collection('organizations').doc(organization.uid).delete()),
    ]);
  }

  addUserByType$(user: UserModel, organization: OrganizationModel, type: ClaimType) {
    const userRef = this.usersRef.doc(user.uid).ref;
    return combineLatest([
      from(this.organizationsPrivatesRef
        .doc(organization.uid)
        .collection(this.utils.getClaimBoolName(type))
        .doc(user.uid).set({
          [this.utils.getClaimBoolName(type)]: true,
          userUid: user.uid,
          userRef,
        }, {merge: true}),
      ),
      from(this.usersRef
        .doc(user.uid)
        .collection('organizations').doc(organization.uid)
        .set({
          [this.utils.getClaimBoolName(type)]: true,
          organizationUid: organization.uid,
          organizationRef: organization.ref,
        }, {merge: true}),
      ),
    ]);
  }

  getAll$(): Observable<OrganizationModel[]> {
    return this.organizationsRef.valueChanges();
  }

  getOrganizationsOfAnUser$(user: UserModel): Observable<any[]> {
    return this.usersRef
      .doc(user.uid)
      .collection('organizations', ref => ref.where('isUser', '==', true))
      .valueChanges();
  }
}
