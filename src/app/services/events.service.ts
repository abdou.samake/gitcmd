import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, from, Observable, of } from 'rxjs';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { EventModel, EventStateEnum, FullEventModel } from '../models/eventModel';
import { OrganizationLink, OrganizationModel } from '../models/organization.model';
import { UserModel } from '../models/user.model';
import { WorkshopModel } from '../models/workshop.model';
import { EventService } from './event.service';
import FieldValue = firebase.firestore.FieldValue;

@Injectable()
export class EventsService {

  constructor(
    private afs: AngularFirestore,
    private nzMessageService: NzMessageService,
    private eventService: EventService,
  ) {
  }

  createWorkshop$(workshop: WorkshopModel, creator: UserModel, organization: OrganizationModel) {
    return from(this.afs.collection('workshops').add({
        ...workshop,
        creatorUid: creator.uid,
        creatorRef: this.afs.collection('users').doc(creator.uid).ref,
        organizationUid: organization.uid,
        organizationRef: organization.ref,
        creationDate: FieldValue.serverTimestamp(),
      }),
    )
      .pipe(
        switchMap((ref: DocumentReference) => from(ref.set({uid: ref.id, ref}, {merge: true}))),
        tap((x) => this.nzMessageService.success('Atelier créé avec susccès', {nzDuration: 2000})),
      );
  }


  getWorkshopById$(workshopUid: string): Observable<WorkshopModel> {
    return this.afs.collection('workshops').doc<WorkshopModel>(workshopUid).valueChanges();
  }


  createEvent$(
    event: EventModel,
    creator: UserModel,
    organization: OrganizationModel,
    workshop: WorkshopModel,
  ): Observable<[DocumentReference, any]> {
    return from(this.afs.collection('events').add({
        ...event,
        creatorUid: creator.uid,
        creatorRef: this.afs.collection('users').doc(creator.uid).ref,
        workshopUid: workshop.uid,
        workshopRef: workshop.ref,
        organizationUid: organization.uid,
        organizationRef: organization.ref,
        creationDate: FieldValue.serverTimestamp(),
        state: EventStateEnum.scheduled,
      }),
    )
      .pipe(
        switchMap((eventRef: DocumentReference) =>
          combineLatest([
            from(eventRef.set({uid: eventRef.id, eventRef}, {merge: true})).pipe(map(() => eventRef)),
            from(this.afs
              .collection('eventsPrivates')
              .doc(eventRef.id)
              .collection('isManager')
              .doc(creator.uid)
              .set({
                userUid: creator.uid,
                userRef: this.afs.collection('users').doc(creator.uid).ref,
              }, {merge: true})),
          ])),
        tap((x) => this.nzMessageService.success('Événement créé avec susccès', {nzDuration: 2000})),
      );
  }

  getAllMyEvents$(user: UserModel): Observable<FullEventModel[]> {
    return this.afs.collection('users').doc(user.uid).collection('events').valueChanges()
      .pipe(
        switchMap(events => events?.length > 0 ? combineLatest(
          events.map(event => this.eventService.getFullEventById$(event.eventUid)),
          ) : of([]),
        ),
      );
  }

  getAllEventsOfAManager$(user: UserModel) {
    return this.afs
      .collection('users')
      .doc(user.uid)
      .collection('events', ref =>
        ref
          .where('isManager', '==', true),
      )
      .valueChanges()
      .pipe(
        switchMap(events => events?.length > 0 ? combineLatest(
          events.map(event => this.eventService.getFullEventById$(event.eventUid)),
          ) : of([]),
        ),
      );
  }

  getAllOpenEvents$(user: UserModel, alreadySubEvents: FullEventModel[]): Observable<FullEventModel[]> {
    return from(this.afs
      .collection('users').doc(user.uid)
      .collection<OrganizationLink>('organizations', ref => ref.where('isUser', '==', true))
      .valueChanges())
      .pipe(
        switchMap((organizationsLinks: OrganizationLink[]) =>
          organizationsLinks?.length > 0
            ? combineLatest(
            organizationsLinks.map((link: OrganizationLink) => this.afs
              .collection<EventModel>('events', ref => ref
                .where('organizationUid', '==', link.organizationUid)
                .where('published', '==', true)
                .where('state', '==', 'scheduled'),
              )
              .valueChanges()
            ),
            )
            : of([]),
        ),
        map((data: EventModel[][]) => data.reduce((acc, value) => acc.concat(value), [])),
        map(data => data.filter(value => !!value)),
        switchMap((events: EventModel[]) => events.length > 0 ? combineLatest(
          events.map(event => this.eventService.getFullEventById$(event.uid)),
          ) : of([]),
        ),
        map((events: FullEventModel[]) => events
          .filter(event => !alreadySubEvents.some(alreadySubEvent => alreadySubEvent.event.uid === event.event.uid)),
        ),
      );
  }

}
