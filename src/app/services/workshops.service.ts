import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { WorkshopModel } from '../models/workshop.model';

@Injectable({
  providedIn: 'root',
})
export class WorkshopsService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }


  getAllMyWorkshops$(user: UserModel): Observable<WorkshopModel[]> {
    return this.afs
      .collection('users').doc(user.uid)
      .collection<WorkshopModel>('organizations', ref => ref.where('isUser', '==', true))
      .valueChanges();

  }
}
