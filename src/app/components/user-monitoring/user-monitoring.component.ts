import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { MonitoringModel } from '../../models/monitoring.model';
import { UserMonitoringService } from './user-monitoring.service';

@Component({
  selector: 'app-user-monitoring',
  templateUrl: './user-monitoring.component.html',
  styleUrls: ['./user-monitoring.component.scss'],
})
export class UserMonitoringComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  @Input() data: MonitoringModel;

  constructor(
    private userMonitoringService: UserMonitoringService,
  ) {
  }

  ngOnInit(): void {
    this.userMonitoringService.getUserMonitoring$(this.data)
      .pipe(
        takeUntil(this.destroy$),
        tap((data) => this.data = data ? data : this.data),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  saveUserMonitoring(data: MonitoringModel) {
    this.userMonitoringService.saveUserMonitoring$(data).subscribe();
  }

}
