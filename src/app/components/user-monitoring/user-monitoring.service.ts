import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { MonitoringModel } from '../../models/monitoring.model';

@Injectable({
  providedIn: 'root',
})
export class UserMonitoringService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }

  getUserMonitoring$(data: MonitoringModel): Observable<MonitoringModel> {
    return this.afs
      .collection('events').doc(data.event.uid)
      .collection('users').doc<MonitoringModel>(data.uid).valueChanges();
  }

  saveUserMonitoring$(data: MonitoringModel): Observable<void> {
    return from(this.afs
      .collection('events').doc(data.event.uid)
      .collection('users').doc<MonitoringModel>(data.uid)
      .set(data, {merge: true}));
  }

}
