import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NzCheckboxModule, NzInputModule, NzRadioModule } from 'ng-zorro-antd';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { UserMonitoringComponent } from './user-monitoring.component';


@NgModule({
  declarations: [
    UserMonitoringComponent,
  ],
  exports: [
    UserMonitoringComponent,
  ],
    imports: [
        CommonModule,
        NzRadioModule,
        FormsModule,
        NzSpaceModule,
        NzCheckboxModule,
        NzInputModule,
    ],
})
export class UserMonitoringModule {
}
