import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { MonitoringModel } from '../../models/monitoring.model';
import { UserModel } from '../../models/user.model';
import { SatisfactionSurveyService } from './satisfaction-survey.service';

@Component({
  selector: 'app-satisfaction-survey',
  templateUrl: './satisfaction-survey.component.html',
  styleUrls: ['./satisfaction-survey.component.scss'],
})
export class SatisfactionSurveyComponent implements OnInit, OnDestroy {

  @Input() user: UserModel;
  @Input() eventUid: string;

  eventAndUser: MonitoringModel;

  destroy$: Subject<boolean> = new Subject<boolean>();

  surveyForm: FormGroup;

  constructor(
    private satisfactionSurveyService: SatisfactionSurveyService,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.satisfactionSurveyService.getEventAndUserData$(this.user, this.eventUid)
      .pipe(
        takeUntil(this.destroy$),
        tap((x) => this.eventAndUser = x),
        tap((x) => this._iniForm(this.eventAndUser)),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onSubmit() {
    this.eventAndUser.survey = {...this.eventAndUser.survey, ...this.surveyForm.value};
    this.satisfactionSurveyService.saveSurvey$(this.eventAndUser)
      .subscribe();
  }

  private _iniForm(eventAndUser: MonitoringModel) {
    this.surveyForm = this.fb.group({
      participated: [eventAndUser?.survey?.participated ?? false, [Validators.required]],
      difficulties: [eventAndUser?.survey?.difficulties ?? false, [Validators.required]],
      videoQuality: [eventAndUser?.survey?.videoQuality ?? 5, [Validators.required]],
      soundQuality: [eventAndUser?.survey?.soundQuality ?? 5, [Validators.required]],
      usability: [eventAndUser?.survey?.usability ?? 5, [Validators.required]],
      referral: [eventAndUser?.survey?.referral ?? 5, [Validators.required]],
    });
  }

}
