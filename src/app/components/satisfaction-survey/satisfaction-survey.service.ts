import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { combineLatest, from, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { EventModel } from '../../models/eventModel';
import { MonitoringModel } from '../../models/monitoring.model';
import { SurveyModel } from '../../models/survey.model';
import { UserLink, UserModel } from '../../models/user.model';
import { WorkshopModel } from '../../models/workshop.model';

@Injectable({
  providedIn: 'root',
})
export class SatisfactionSurveyService {

  constructor(
    private afs: AngularFirestore,
  ) {
  }

  getEventAndUserData$(user: UserModel, eventUid: string): Observable<MonitoringModel> {
    return combineLatest([
      this.afs.collection('events').doc<EventModel>(eventUid).valueChanges(),
      this.afs.collection('eventsPrivates').doc(eventUid).collection('surveys').doc<SurveyModel>(user.uid).valueChanges(),
      this.afs.collection('eventsPrivates').doc(eventUid).collection<UserLink>('isManager').valueChanges()
        .pipe(switchMap((userLinks) => combineLatest(
          userLinks.map(link => this.afs.collection('users').doc<UserModel>(link.userUid).valueChanges())))
        ),
    ]).pipe(
      map(([event, survey, organizers]: [EventModel, SurveyModel, UserModel[]]) => ({event, survey, ...user, organizers})),
      switchMap((eventMonitoring: MonitoringModel) => this.afs
        .collection('workshops')
        .doc<WorkshopModel>(eventMonitoring.event.workshopUid)
        .valueChanges().pipe(map(workshop => ({...eventMonitoring, workshop})))),
    );
  }

  saveSurvey$(eventAndUser: MonitoringModel) {
    return from(this.afs
      .collection('eventsPrivates')
      .doc(eventAndUser.event.uid)
      .collection('surveys')
      .doc(eventAndUser.uid)
      .set(eventAndUser.survey, {merge: true}),
    );
  }
}
