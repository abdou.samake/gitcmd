import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NzButtonModule,
  NzDescriptionsModule,
  NzDividerModule,
  NzFormModule, NzListModule,
  NzRadioModule,
  NzRateModule,
  NzSpinModule,
} from 'ng-zorro-antd';
import { PipesModule } from '../../pipes/pipes.module';
import { SatisfactionSurveyComponent } from './satisfaction-survey.component';

@NgModule({
  declarations: [
    SatisfactionSurveyComponent,
  ],
  exports: [
    SatisfactionSurveyComponent,
  ],
  imports: [
    CommonModule,
    NzDescriptionsModule,
    ReactiveFormsModule,
    NzSpinModule,
    NzFormModule,
    NzRadioModule,
    NzDividerModule,
    NzRateModule,
    NzButtonModule,
    PipesModule,
    NzListModule,
  ],
})
export class SatisfactionSurveyModule {
}
