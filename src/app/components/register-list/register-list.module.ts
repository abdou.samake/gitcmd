import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NzCardModule, NzGridModule, NzSwitchModule } from 'ng-zorro-antd';
import { RegisterListComponent } from './register-list.component';


@NgModule({
  declarations: [
    RegisterListComponent,
  ],
  exports: [
    RegisterListComponent,
  ],
  imports: [
    CommonModule,
    NzGridModule,
    NzCardModule,
    NzSwitchModule,
    FormsModule,
  ],
})
export class RegisterListModule {
}
