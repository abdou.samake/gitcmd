import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { OrganizationModel } from '../../models/organization.model';
import { UserModel } from '../../models/user.model';
import { OrganizationService } from '../../services/organization.service';

@Component({
  selector: 'app-register-list',
  templateUrl: './register-list.component.html',
  styleUrls: ['./register-list.component.scss'],
})
export class RegisterListComponent implements OnInit, OnDestroy {

  @Input() user: UserModel;

  organizations: OrganizationModel[];
  registeredOrganizations: any[];

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private organizationService: OrganizationService,
  ) {
  }

  ngOnInit(): void {
    this.organizationService.getAll$()
      .pipe(
        takeUntil(this.destroy$),
        tap((organizations) => this.organizations = organizations),
      )
      .subscribe();

    this.organizationService.getOrganizationsOfAnUser$(this.user)
      .pipe(
        takeUntil(this.destroy$),
        tap((registeredOrganizations) => this.registeredOrganizations = registeredOrganizations),
      )
      .subscribe();
  }

  toggleUser(org: OrganizationModel) {
    if (this.testIfUserAlreadyInOrganization(org)) {
      this.organizationService.removeUserFromOrganization$(this.user, org, 'isUser').subscribe();
    } else {
      this.organizationService.addUserByType$(this.user, org, 'isUser').subscribe();
    }

  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  testIfUserAlreadyInOrganization(organization: OrganizationModel): boolean {
    if (this.registeredOrganizations?.length > 0 && organization.uid) {
      return this.registeredOrganizations?.some(org => org.organizationUid === organization.uid);
    }
    return false;
  }

}
