import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {
  NzAlertModule,
  NzAvatarModule, NzBreadCrumbModule,
  NzIconModule,
  NzLayoutModule,
  NzMenuModule,
  NzPageHeaderModule,
  NzTagModule,
} from 'ng-zorro-antd';
import { PipesModule } from '../../pipes/pipes.module';
import {MainLayoutComponent} from './main-layout/main-layout.component';

@NgModule({
  declarations: [
    MainLayoutComponent,
  ],
  exports: [
    MainLayoutComponent,
  ],
    imports: [
        CommonModule,
        NzLayoutModule,
        NzMenuModule,
        NzIconModule,
        RouterModule,
        NzPageHeaderModule,
        NzAvatarModule,
        NzTagModule,
        NzBreadCrumbModule,
        NzAlertModule,
        PipesModule,
    ],
})
export class LayoutsModule {
}
