import { Component, Input, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { AlertHeaderModel, BannerHeaderModel, PageHeaderTagModel } from '../../../models/page-header-tag.model';
import { UserModel } from '../../../models/user.model';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {

  isCollapsed = false;
  @Input() avatar: string;
  @Input() title: string;
  @Input() subTitle: string;
  @Input() tag: PageHeaderTagModel;
  @Input() breadCrumbs: string[];
  @Input() banners: BannerHeaderModel[];
  @Input() alerts: AlertHeaderModel[];
  @Input() initials: string;

  user: UserModel;

  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.userService.getUser$()
      .pipe(
        tap((user: UserModel) => this.user = user),
      )
      .subscribe();
  }


  signOut() {
    this.authService.signOut$()
      .pipe(
        tap((x) => window.location.reload()),
      )
      .subscribe();
  }


}
