import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzTagModule, NzTypographyModule } from 'ng-zorro-antd';
import { SeatsDisplayerComponent } from './seats-displayer.component';



@NgModule({
  declarations: [SeatsDisplayerComponent],
  exports: [SeatsDisplayerComponent],
  imports: [
    CommonModule,
    NzTypographyModule,
    NzTagModule,
  ],
})
export class SeatsDisplayerModule { }
