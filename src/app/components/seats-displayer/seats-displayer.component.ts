import { Component, Input, OnInit } from '@angular/core';
import { FullEventModel } from '../../models/eventModel';

@Component({
  selector: 'app-seats-displayer',
  templateUrl: './seats-displayer.component.html',
  styleUrls: ['./seats-displayer.component.scss']
})
export class SeatsDisplayerComponent implements OnInit {

  @Input() fullEvent: FullEventModel;
  constructor() { }

  ngOnInit(): void {
  }

}
