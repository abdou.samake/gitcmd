import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeatsDisplayerComponent } from './seats-displayer.component';

describe('SeatsDisplayerComponent', () => {
  let component: SeatsDisplayerComponent;
  let fixture: ComponentFixture<SeatsDisplayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeatsDisplayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeatsDisplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
