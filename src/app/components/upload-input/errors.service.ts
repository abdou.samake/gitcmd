import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

  static testFilesTypes(file: File, acceptedTypes: string[]): boolean {
    if (!file) {
      throw new Error('missing file or wrong file argument');
    }
    if (!acceptedTypes || acceptedTypes.some(type => type === '*')) {
      return true;
    }
    return acceptedTypes.some(type => type === file.type);
  }

  static testFilesMaxSize(file: File, maxSize: number): boolean {
    if (!file || !file.size) {
      throw new Error('missing file or wrong file argument');
    }
    if (!maxSize) {
      return true;
    }
    return file.size <= maxSize;
  }

}
