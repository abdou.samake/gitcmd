import {ElementRef, Injectable} from '@angular/core';
import {filter, finalize, map, pairwise, switchMap, take, tap, throttleTime} from 'rxjs/operators';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {BehaviorSubject, combineLatest, Observable, of, Subject} from 'rxjs';
import {FileToUpload, LimitedFileToUpload} from './upload-input.model';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  files: FileToUpload[] = [];
  path = '';
  uploadAllFilesPendingState$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  totalSize$: BehaviorSubject<number> = new BehaviorSubject(0);
  currentPercentage$: BehaviorSubject<number> = new BehaviorSubject(0);
  speed$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  end$: Subject<boolean>;

  totalPercentage = 0;
  htmlInputElementElementRef: ElementRef<HTMLInputElement>;
  private _FORCED_NAME = '';

  constructor(
    private storage: AngularFireStorage,
  ) {
  }

  openFileDialog(): void {
    this.htmlInputElementElementRef.nativeElement.click();
  }

  setForcedName(value: string) {
    this._FORCED_NAME = value;
  }

  resetForcedName() {
    this._FORCED_NAME = '';
  }

  calculatePercentage() {
    this.currentPercentage$.next(Math.round(this.files
      .reduce((acc, next) => acc + (isNaN(next.percentage) ? 0 : next.percentage), 0) / this.totalPercentage * 100)
    );
  }

  resetFiles() {
    this.files = [];
    this.resetNativeElement();
    this.resetForcedName();
  }

  registerNativeElement(nativeElement: ElementRef) {
    this.htmlInputElementElementRef = nativeElement;
  }

  setFilesValuesAtIndex(index: number, value: FileToUpload) {
    this.files[index] = value;
    this.totalSize$.next(this.files.reduce((acc, next) => acc + next.file.size, 0));
    this.totalPercentage = this.files.length * 100;
  }

  resetNativeElement() {
    if (this.htmlInputElementElementRef) {
      this.htmlInputElementElementRef.nativeElement.value = '';
    }
  }

  uploadAllFiles$(): Observable<FileToUpload[]> {
    this.uploadAllFilesPendingState$.next(true);
    return combineLatest(
      this.files.map((file: FileToUpload, index: number) => this._uploadOneFileWithoutPrivates$(index)))
      .pipe(
        take(1),
        map(() => this.files),
        finalize(() => {
          this.uploadAllFilesPendingState$.next(false);
        })
      );
  }

  getSpeed$() {
    return this.speed$.asObservable();
  }

  calculateSpeed$() {
    return this.totalSize$
      .pipe(
        switchMap((totalSize: number) => this.currentPercentage$.pipe(
          throttleTime(1000),
          map((percentage: number) => percentage * totalSize),
          pairwise(),
          map(([value1, value2]) => value2 - value1),
          map(value => Math.round(value / 1000 / 1000 / 10) / 10),
          map(value => value + ' Mbit/s'),
          tap(speed => this.speed$.next(speed)),
        )),
      );
  }

  makeLimitedFile(
    ownerUid: string,
    file: FileToUpload,
    type?: string,
    fileName?: string,
    size?: number,
  ): LimitedFileToUpload {
    return {
      file: fileName || file.file.name || '',
      downloadUrl: file.downloadUrl,
      ownerUid,
      size: file.file.size || size,
      name: fileName || file.file.name || '',
      type: type || file.file.type || '',
    };
  }

  private _uploadOneFileWithoutPrivates$(index: number) {
    const filePath: string = this.path + (this._FORCED_NAME ? this._FORCED_NAME : this.files[index].file.name);
    this.files[index].ref = this.storage.ref(filePath);

    return of(this.files[index].ref.put(this.files[index].file, {
      contentType: this.files[index].file.type,
    }))
      .pipe(
        take(1),
        map((task$: AngularFireUploadTask) => this.files[index].task$ = task$)
      )
      .pipe(
        switchMap(() => this._manageOneFileUpload$(index)));
  }

  private _manageOneFileUpload$(index: number) {
    this.end$ = new Subject<boolean>();
    const emitUrlWhenFinished$ = this.end$
      .pipe(
        tap((finished: boolean) => this.files[index].finished = finished),
        switchMap(() => this.files[index].ref.getDownloadURL()),
        tap(url => this.files[index].downloadUrl = url),
      );

    return combineLatest([
      emitUrlWhenFinished$,
      this.files[index].task$.percentageChanges()
        .pipe(
          tap(percentage => this.files[index].percentage = percentage),
          tap(() => this.calculatePercentage()),
        ),
      this.files[index].task$.snapshotChanges()
        .pipe(
          finalize(() => {
            this.end$.next(true);
            this.end$.complete();
          })
        )

    ]);
  }
}
