import {AngularFireStorageReference, AngularFireUploadTask} from '@angular/fire/storage';
import {DocumentReference} from '@angular/fire/firestore';

export interface FileToUpload {
  data: string | ArrayBuffer | null;
  file: File;
  percentage?: number;
  finished?: boolean;
  ref?: AngularFireStorageReference;
  task$?: AngularFireUploadTask;
  downloadUrl?: string;
}

export interface LimitedFileToUpload {
  file: string;
  type?: string;
  name?: string;
  downloadUrl?: string;
  ref?: DocumentReference;
  ownerUid: string;
  size: number;
}


export interface ProviderAsset extends LimitedFileToUpload {
  state: ProviderAssetEnum;
  uploadedAt: any;
  index: number;
  accepted: boolean;
  inValidation: boolean;
  uid?: string;
}

export enum ProviderAssetEnum {
  pendingPMValidation = 'PENDING_PM_VALIDATION',
  submittedToClient = 'SUBMITTED_TO_CLIENT',
  refusedByPM = 'REFUSED',
  refusedByClient = 'REFUSED_BY_CLIENT',
  validated = 'VALIDATED'
}
