import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploadInputComponent} from './upload-input.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NzButtonModule, NzIconModule, NzProgressModule, NzSpinModule} from 'ng-zorro-antd';

@NgModule({
  declarations: [UploadInputComponent],
  exports: [UploadInputComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzIconModule,
    NzProgressModule,
    NzSpinModule,
  ]
})
export class UploadInputModule {
}
