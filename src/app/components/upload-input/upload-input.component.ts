import {
  AfterViewInit,
  Component, ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {UploadService} from './upload.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileToUpload} from './upload-input.model';
import {BehaviorSubject, Observable} from 'rxjs';
import {ErrorsService} from './errors.service';

@Component({
  selector: 'app-upload-input',
  templateUrl: './upload-input.component.html',
  styleUrls: ['./upload-input.component.scss']
})
export class UploadInputComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() transclude: TemplateRef<any>;
  @Input() mainForm: FormGroup;
  @Input() multi = true;
  @Input() acceptType: string[] = ['*'];
  @Input() maxSize: number = null;
  @Input() placeholder = 'Cliquez ici pour ajouter des fichiers';
  @Output() emitError: EventEmitter<string> = new EventEmitter();
  @ViewChild('uploadInput', {static: true}) uploadInputElement: ElementRef;

  speed$: Observable<string> = this.uploadService.getSpeed$();

  uploadForm: FormGroup;
  uploadAllFilesPendingState$: BehaviorSubject<boolean> = this.uploadService.uploadAllFilesPendingState$;
  hovered = false;

  constructor(
    public uploadService: UploadService,
    private errorsService: ErrorsService,
    private fb: FormBuilder,
  ) {
    this.uploadForm = this.fb.group({mainImages: [null, Validators.required]});
  }

  @Input()
  set path(url) {
    this.uploadService.path = url;
  }

  get files(): FileToUpload[] {
    return this.uploadService.files;
  }

  get thumbs() {
    return this.mainForm.get('thumbs');
  }

  ngOnInit(): void {
    this.uploadService.resetForcedName();
    if (!this.mainForm) {
      throw new Error('a main form should be passed in mainForm @input');
    }
    if (this.mainForm && !this.mainForm.get('thumbs')) {
      throw new Error('mainForm should contains a "thumbs" controller');
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.uploadService.resetFiles();
    this.uploadService.resetNativeElement();
  }

  ngAfterViewInit(): void {
    this.uploadService.registerNativeElement(this.uploadInputElement);
  }

  onDragLeave(event) {
    this.hovered = false;
    event.stopPropagation();
    event.preventDefault();
  }

  onDragOver(event) {
    this.hovered = true;
    event.stopPropagation();
    event.preventDefault();
  }

  onDrop(event) {
    this.hovered = false;
    event.preventDefault();
    const data = event.dataTransfer.files;
    this.selectFiles(data, true);
  }

  selectFiles(event, onDrop?: boolean) {
    let fileList: FileList;
    if (onDrop) {
      fileList = event;
    } else {
      fileList = event.target.files;
    }
    if (fileList && fileList.length > 0) {
      if (!this.thumbs.value) {
        this.thumbs.setValue(this.files || fileList);
      }

      Array.from(fileList)
        .map((file: File) => [new FileReader(), file])
        .map(([reader, file]: [FileReader, File], index: number) => {
          if (this._manageErrors(file)) {
            reader.onloadstart = (ev: ProgressEvent) => {
              this.uploadService.setFilesValuesAtIndex(index, {
                data: (ev.target as FileReader).result,
                file,
              });
              this.thumbs.setValue(this.files);
            };
          } else {
            reader.abort();
            this.uploadService.resetFiles();
          }
          return [reader, file];
        })
        .map(([reader, file]: [FileReader, File]) => reader.readAsDataURL(file));
      this.uploadService.resetNativeElement();
    }
  }

  removeFile(index: number) {
    this.uploadService.resetNativeElement();
    this.files.splice(index, 1);
    if (this.files && this.files.length === 0) {
      this.uploadService.resetFiles();
      this.uploadForm.reset();
      this.uploadForm.markAsPristine();
    }
    this.thumbs.setValue(null);
  }

  private _manageErrors(file: File): boolean {
    if (!ErrorsService.testFilesTypes(file, this.acceptType)) {
      console.error(`Le fichier ${file.name} n'est pas du bon type `);
      this.emitError.emit(`Le fichier ${file.name} n'est pas du bon type. Authorisés : ${this.acceptType.toString()}`);
      this.mainForm.reset();
      return false;
    }
    if (!ErrorsService.testFilesMaxSize(file, this.maxSize)) {
      this.emitError.emit(`Le fichier ${file.name} dépasse la taille maximum autorisée (${(this.maxSize / 1000 / 1000).toFixed(2)} Mo)`);
      console.error(`Le fichier ${file.name} dépasse la taille maximum autorisée `);
      this.mainForm.reset();
      return false;
    }
    return true;
  }

}
