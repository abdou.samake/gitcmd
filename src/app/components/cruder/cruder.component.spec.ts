import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CruderComponent } from './cruder.component';

describe('CruderComponent', () => {
  let component: CruderComponent;
  let fixture: ComponentFixture<CruderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CruderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CruderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
