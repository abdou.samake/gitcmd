import { NzTableQueryParams } from 'ng-zorro-antd';
import { NzButtonType } from 'ng-zorro-antd/button/button.component';
import { Observable } from 'rxjs';

export interface CruderServiceModel<T, M> {
  filter1?: FilterInterface[];
  keys: FilterKey[];

  save?: (T, key: FilterKey, test: any) => any;
  getByType$(type: M, filters: NzTableQueryParams, other?: any): Observable<T[]>;
}

export interface CruderComponentModel {
  filter1: FilterInterface[];
  keys: FilterKey[];
}


export interface FilterInterface {
  text: string;
  value: string;
}

export interface FilterKey {
  columnName: string;
  databaseKey?: string;
  nzSortFn?: boolean;
  nzFilterFn?: boolean;
  filter?: FilterInterface[];
  withButtons?: boolean;
  withMonitoring?: boolean;
  buttons?: ButtonTable[];
  condition?: (args?: any) => boolean;
  dataType?: DataType;
}

export interface ServerQuery extends NzTableQueryParams {
  sortField: string;
  sortOrder: string;
}

export interface ButtonTable {
  text?: string;
  type: NzButtonType;
  action: (args?: any) => any;
  iconThemeType?: ThemeType;
  iconName?: string;
  tooltipText?: string;
  condition: (args?: any) => boolean;
}


export type ThemeType = 'fill' | 'outline' | 'twotone';

export type DataType = 'text' | 'date' | 'dateWithHours';
