import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
  CruderComponentModel,
  CruderServiceModel,
  FilterInterface,
  FilterKey,
  ServerQuery,
} from './cruderServiceModel';

@Component({
  selector: 'app-cruder',
  templateUrl: './cruder.component.html',
  styleUrls: ['./cruder.component.scss'],
})
export class CruderComponent implements OnInit, OnChanges, OnDestroy, CruderComponentModel {

  @Input() service: CruderServiceModel<any, any>;
  @Input() type: any;
  @Input() other: any;

  destroy$: Subject<boolean> = new Subject<boolean>();

  total = 1;
  list: any[] = [];
  loading = true;
  pageSize = 10;
  pageIndex = 1;
  filter1: FilterInterface[];
  keys: FilterKey[];
  test = null;

  constructor(
  ) {
  }

  ngOnInit(): void {
    this.filter1 = this.service.filter1;
    this.keys = this.service.keys;
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  loadDataFromServer(params: ServerQuery): void {
    this.loading = true;
    this.service.getByType$(this.type, params, this.other)
      .pipe(
        tap((x) => {
          this.loading = false;
          this.total = x.length;
          this.list = x;
        }),
      )
      .subscribe();
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const {pageSize, pageIndex, sort, filter} = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer({...params, pageIndex, pageSize, sortField, sortOrder, filter});
  }

}
