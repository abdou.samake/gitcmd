import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NzButtonModule, NzIconModule, NzInputModule, NzTableModule, NzToolTipModule } from 'ng-zorro-antd';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { UserMonitoringModule } from '../user-monitoring/user-monitoring.module';
import { CruderComponent } from './cruder.component';

@NgModule({
  declarations: [
    CruderComponent,
  ],
  exports: [
    CruderComponent,
  ],
  imports: [
    CommonModule,
    NzButtonModule,
    NzTableModule,
    NzIconModule,
    NzSpaceModule,
    NzToolTipModule,
    UserMonitoringModule,
    FormsModule,
    NzInputModule,
  ],
})
export class CruderModule {
}
