import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd';
import { of, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil, tap } from 'rxjs/operators';
import { FileToUpload } from '../components/upload-input/upload-input.model';
import { UploadService } from '../components/upload-input/upload.service';
import { AlertHeaderModel } from '../models/page-header-tag.model';
import { PatientValidationState } from '../models/patient-validation-state.model';
import { UserModel } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { ModalsManagerService } from '../services/modals-manager.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  newPassword: FormGroup;
  user: UserModel;
  destroy$: Subject<boolean> = new Subject<boolean>();
  alerts: AlertHeaderModel[];

  isLoading = true;
  editPicture = false;
  passwordVisible = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private message: NzMessageService,
    private userService: UserService,
    private uploadService: UploadService,
    private modalsManagerService: ModalsManagerService,
  ) {
  }

  ngOnInit(): void {
    this.getUserById();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getUserById() {
    this.isLoading = true;
    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        tap(() => this.isLoading = false),
        tap((user: UserModel) => this.user = user),
        tap(() => {
          if (this.user.validationState === PatientValidationState.toComplete) {
            this.alerts = [{
              showIcon: true,
              description: 'Vous devez compléter les informations de votre compte avant de continuer',
              message: 'Compte incomplet',
              type: 'warning',
            }];
            this.modalsManagerService
              .info(
                'Nous avons besoin de votre aide',
                'Vous devez compléter les informations de votre compte avant de continuer',
              );
          } else {
            this.alerts = [];
          }
        }),
        tap(x => this._initForm(this.user)),
      )
      .subscribe();
  }

  cancel(): void {
    this.message.info('Annulé');
  }

  updateUser() {
    this.user = {...this.user, ...this.userForm.value};
    this.userService.updateUserById$(this.user.uid, this.user)
      .pipe(
        tap((x) => this.modalsManagerService.info('Modifications prises en compte', 'vous pouvez continuer')),
      )
      .subscribe();
  }

  confirm(password: string): void {
    this._updatePassword(password);
  }

  upload() {
    this.uploadService.setForcedName(this.user.uid);
    this.uploadService.uploadAllFiles$()
      .pipe(
        switchMap(([file]: FileToUpload[]) =>
          this.userService.savePictureProfile$(this.user.uid, file.downloadUrl)),
        tap(() => this.message.success('image mise à jour')),
        tap(() => this.uploadService.resetForcedName()),
        tap(() => this.editPicture = false),
        catchError(err => {
          this.editPicture = false;
          this.message.error(err);
          return of(err);
        }),
      )
      .subscribe();
  }

  displayError(message) {
    this.message.error(message);
  }

  changeEmailValue(data) {
    this.user.notificationByMail = data;
    this.updateUser();
  }

  private _initForm(user: UserModel) {
    this.userForm = this.fb.group({
      gender: [user.gender || null, [Validators.required]],
      firstName: [user.firstName || null, [Validators.required]],
      lastName: [user.lastName || null, [Validators.required]],
      zip: [user.zip || null, [Validators.required]],
      phone: [user.phone || null, [Validators.required]],
      birthday: [user.birthday || null, [Validators.required]],
      thumbs: [''],
      photoURL: [user.photoURL || null],
    });

    this.newPassword = this.fb.group({
      newPassword: ['' || null, [Validators.required, Validators.minLength(6)]],
    });
  }

  private _updatePassword(password: string) {
    this.authService.changePassword(password)
      .pipe(
        tap(() => this.message.success('Votre mot de passe a été modifié')),
      )
      .subscribe();
  }

}
