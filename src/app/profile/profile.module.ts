import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NzAlertModule,
  NzAvatarModule, NzButtonModule,
  NzFormModule,
  NzGridModule,
  NzInputModule, NzPopconfirmModule,
  NzSpinModule,
  NzSwitchModule,
  NzMessageServiceModule, NzDividerModule, NzBreadCrumbModule, NzSelectModule, NzIconModule,
} from 'ng-zorro-antd';
import { LayoutsModule } from '../components/layouts/layouts.module';
import { UploadInputModule } from '../components/upload-input/upload-input.module';
import { PipesModule } from '../pipes/pipes.module';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';


@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    LayoutsModule,
    NzSpinModule,
    NzGridModule,
    NzAlertModule,
    NzAvatarModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzSwitchModule,
    FormsModule,
    NzButtonModule,
    NzPopconfirmModule,
    UploadInputModule,
    NzAlertModule,
    NzMessageServiceModule,
    NzDividerModule,
    NzBreadCrumbModule,
    NzSelectModule,
    NzIconModule,
    PipesModule,
  ],
})
export class ProfileModule { }
