import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { ModalsManagerService } from '../../services/modals-manager.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss', '../auth.component.scss'],
})
export class ForgotComponent implements OnInit {

  email: string;

  constructor(
    private auth: AuthService,
    private modalsManagerService: ModalsManagerService,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.auth
      .resetEmail$(this.email)
      .pipe(
        tap((x) => this.modalsManagerService.info('Demande reçue', 'Regardez vos mails.')),
        catchError(err => {
          this.modalsManagerService.error(err.message);
          return of(err);
        }),
      )
      .subscribe();

  }

}
