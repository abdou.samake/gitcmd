import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseError } from 'firebase/app';
import { of, Subject } from 'rxjs';
import { catchError, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { UserModel } from '../../models/user.model';
import { AuthService } from '../../services/auth.service';
import { ModalsManagerService } from '../../services/modals-manager.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  index = 0;
  accountCreated = false;
  public createForm: FormGroup;
  public registerForm: FormGroup;

  user: UserModel;

  constructor(
    private fb: FormBuilder,
    private modalsManagerService: ModalsManagerService,
    private authService: AuthService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    const regexPhone = /^((\+)33|0)[1-9](\d{2}){4}$/;
    this.createForm = this.fb.group({
      civility: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      birth_date: ['', Validators.required],
      zip_code: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern(regexPhone)]],
      reseau_id: ['', Validators.required],
    });

    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      is_adult: [false, Validators.requiredTrue],
      is_cgu_validated: [false, Validators.requiredTrue],
    });

    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        filter(user => !!user.uid),
        tap((user) => this.user = user),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  createAccountWithEmailAndPassword() {
    if (this.registerForm.invalid) {
      this.registerForm.markAllAsTouched();
      this.modalsManagerService.errorOnForm(this.registerForm);
      return;
    }
    const {email, password} = this.registerForm.value;
    this.authService.createAccountWithEmailAndPassword$(email, password)
      .pipe(
        // switchMap((usercred) => this.userService.),
        map(() => this.modalsManagerService
          .info('Votre compte a bien été créé', `Vous pouvez passer à l'étape suivante.`),
        ),
        switchMap(ref => ref.afterClose.asObservable()),
        tap(() => this.accountCreated = true),
        tap(() => this.index = 1),
        catchError((err: FirebaseError) => {
          this.modalsManagerService.error(err.message);
          return of(err);
        }),
      )
      .subscribe();
  }

  onIndexChange(index: number): void {
    this.index = index;
  }

}
