import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import { NzButtonModule, NzSpinModule, NzStepsModule, NzTypographyModule } from 'ng-zorro-antd';
import { RegisterListModule } from '../../components/register-list/register-list.module';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
    NzStepsModule,
    NzTypographyModule,
    NzButtonModule,
    RegisterListModule,
    NzSpinModule,
  ],
})
export class SignupModule { }
