import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseError } from 'firebase';
import { of, pipe } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { ModalsManagerService } from '../services/modals-manager.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private modalsManagerService: ModalsManagerService,
    private authService: AuthService,
    private router: Router,
    private zone: NgZone,
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      password: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  connectWithEmailAndPassword() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      this.modalsManagerService.errorOnForm(this.loginForm);
      return;
    }
    const {email, password} = this.loginForm.value;
    this.loading = true;
    this.authService.connectWithEmailAndPassword$(email, password)
      .pipe(this.manageConnected$())
      .subscribe();
  }

  signInWithGoogle() {
    this.loading = true;
    this.authService.signInWithGoogle$()
      .pipe(this.manageConnected$())
      .subscribe();
  }

  forgotPassword() {

  }

  private manageConnected$() {
    return pipe(
      tap((x) => this.router.navigate(['/profile'])),
      catchError((err: FirebaseError) => {
        this.modalsManagerService.error(err.message, () => this.loading = false);
        return of(err);
      }),
    );
  }
}
