import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { NzMessageService } from 'ng-zorro-antd';
import { NzTableQueryParams } from 'ng-zorro-antd/table/public-api';
import { Observable, of, pipe } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { CruderServiceModel, FilterInterface, FilterKey } from '../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../models/claims.model';
import { UserModel } from '../models/user.model';
import { UtilsService } from '../services/utils.service';
import DocumentData = firebase.firestore.DocumentData;
import Query = firebase.firestore.Query;

@Injectable()
export class UsersService implements CruderServiceModel<UserModel, ClaimsEnum> {

  filter1: FilterInterface[] = [
    {text: 'male', value: 'monsieur'},
    {text: 'female', value: 'madame'},
  ];

  keys: FilterKey[] = [
    {
      databaseKey: 'lastName',
      columnName: 'nom',
      nzSortFn: true,
    },
    {
      databaseKey: 'firstName',
      columnName: 'Prénom',
    },
    {
      databaseKey: 'birthday',
      columnName: 'Date de naissance',
    },
    {
      databaseKey: 'zip',
      columnName: 'Code postal',
    },
    {
      databaseKey: 'gender',
      nzFilterFn: true,
      columnName: 'genre',
      filter: this.filter1,
    },
    {
      databaseKey: 'email',
      columnName: 'email',
    },
    {
      databaseKey: 'phone',
      columnName: 'Téléphone',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'default',
          action: (user: UserModel) => {
            return this.setUserClaims(ClaimsEnum.isUser, user.uid);
          },
          iconName: 'user',
          tooltipText: 'Passer patient',
          condition: (user: UserModel) => {
            return !user.isUser;
          },
        },
        {
          type: 'default',
          action: (user: UserModel) => {
            return this.setUserClaims(ClaimsEnum.isManager, user.uid);
          },
          iconName: 'cluster',
          tooltipText: 'Passer manager',
          condition: (user: UserModel) => {
            return !user.isManager;
          },
        },
        {
          type: 'primary',
          action: (user: UserModel) => {
            return this.setUserClaims(ClaimsEnum.isPds, user.uid);
          },
          iconName: 'heart',
          tooltipText: 'Passer pds',
          condition: (user: UserModel) => {
            return !user.isPds;
          },
        },
        {
          type: 'danger',
          action: (user: UserModel) => {
            return this.setUserClaims(ClaimsEnum.isAdmin, user.uid);
          },
          iconName: 'alert',
          tooltipText: 'Passer admin',
          condition: (user: UserModel) => {
            return true;
          },
        },
        {
          type: 'default',
          action: (user: UserModel) => {
            return this.router.navigate(['/admin/users', user.uid]);
          },
          iconName: 'eye',
          tooltipText: 'Voir les détails',
          condition: () => true,
        },
      ],
    },

  ];

  constructor(
    private afs: AngularFirestore,
    private aff: AngularFireFunctions,
    private router: Router,
    private nzMessageService: NzMessageService,
    private utils: UtilsService,
  ) {
  }

  setUserClaims(type: ClaimsEnum, userUid: string) {
    this.aff.httpsCallable('do_set_custon_user_claims')({[this.utils.getClaimBooleanName(type)]: true, uid: userUid})
      .pipe(
        tap((x) => this.nzMessageService.success('Droits changés avec succès', {nzDuration: 2000})),
      )
      .subscribe();
  }

  getByType$(type: ClaimsEnum, filters): Observable<UserModel[]> {
    return this.afs
      .collection<UserModel>('users', ref => this.makeFilteredRequest(ref, type, filters))
      .valueChanges();
  }

  private makeFilteredRequest(ref: CollectionReference, type: ClaimsEnum, filters: NzTableQueryParams): Query<DocumentData> {
    let request: Query<DocumentData> = ref
      .where(this.utils.getClaimBooleanName(type), '==', true)
      .orderBy('creationDate', 'desc');
    filters.filter.forEach(filter => {
      filter.value.forEach(value => {
          if (filter.key && value) {
            request = request.where(filter.key, '==', value);
          }
        },
      );
    });
    return request;
  }

  private _manageError() {
    return pipe(
      catchError((err) => {
        console.error('--------------', err);
        return of(err);
      }),
    );
  }

}
