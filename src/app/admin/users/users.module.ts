import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzMessageServiceModule, NzSpinModule, NzTabsModule } from 'ng-zorro-antd';
import { CruderModule } from '../../components/cruder/cruder.module';
import { LayoutsModule } from '../../components/layouts/layouts.module';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';


@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    LayoutsModule,
    NzSpinModule,
    NzTabsModule,
    CruderModule,
    NzMessageServiceModule,
  ],
})
export class UsersModule { }
