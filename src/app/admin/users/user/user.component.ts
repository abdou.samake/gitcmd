import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AlertHeaderModel } from '../../../models/page-header-tag.model';
import { UserModel } from '../../../models/user.model';
import { ModalsManagerService } from '../../../services/modals-manager.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  user: UserModel;
  destroy$: Subject<boolean> = new Subject<boolean>();
  alerts: AlertHeaderModel[];

  isLoading = true;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private modalsManagerService: ModalsManagerService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.getUserById();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  getUserById() {
    this.isLoading = true;
    this.activatedRoute.params
      .pipe(
        takeUntil(this.destroy$),
        map(params => params.uid),
        switchMap(uid => this.userService.getUserById$(uid)),
        tap(() => this.isLoading = false),
        tap((user: UserModel) => this.user = user),
        tap(() => this._initForm(this.user)),
      )
      .subscribe();
  }

  updateUser() {
    this.user = {...this.user, ...this.userForm.value};
    this.userService.updateUserById$(this.user.uid, this.user)
      .pipe(
        tap((x) => this.modalsManagerService.info('Modifications prises en compte', 'vous pouvez continuer')),
      )
      .subscribe();
  }

  private _initForm(user: UserModel) {
    this.userForm = this.fb.group({
      gender: [user.gender || null, [Validators.required]],
      firstName: [user.firstName || null, [Validators.required]],
      lastName: [user.lastName || null, [Validators.required]],
      zip: [user.zip || null, [Validators.required]],
      phone: [user.phone || null, [Validators.required]],
      birthday: [user.birthday || null, [Validators.required]],
      thumbs: [''],
      photoURL: [user.photoURL || null],
    });
  }
}
