import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NzButtonModule,
  NzFormModule,
  NzGridModule,
  NzInputModule,
  NzSelectModule,
  NzSpinModule,
} from 'ng-zorro-antd';
import { LayoutsModule } from '../../../components/layouts/layouts.module';
import { PipesModule } from '../../../pipes/pipes.module';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';


@NgModule({
  declarations: [UserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    NzSpinModule,
    LayoutsModule,
    NzGridModule,
    ReactiveFormsModule,
    NzFormModule,
    NzSelectModule,
    NzInputModule,
    PipesModule,
    NzButtonModule,
  ],
})
export class UserModule {
}
