import { Component, OnInit } from '@angular/core';
import { ClaimsEnum } from '../../../../../backend/functions/doSetCustomUserClaims/functions/src/claims.model';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UsersService],
})
export class UsersComponent implements OnInit {
  claimsEnum = ClaimsEnum;
  isLoading = true;
  type: ClaimsEnum;

  constructor(
    public usersService: UsersService,
  ) {
  }

  ngOnInit(): void {
    this.isLoading = false;
  }
}
