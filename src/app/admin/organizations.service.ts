import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { combineLatest, Observable, of, pipe } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../models/claims.model';
import { OrganizationModel } from '../models/organization.model';

@Injectable({
  providedIn: 'root',
})
export class OrganizationsService implements CruderServiceModel<OrganizationModel, ClaimsEnum> {

  keys: FilterKey[] = [
    {
      databaseKey: 'name',
      columnName: 'Nom',
    },
    {
      databaseKey: 'autoValidation',
      columnName: 'Validation auto',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'default',
          action: (organization: OrganizationModel) => {
            return this.router.navigate(['/admin/organizations', organization.uid]);
          },
          iconName: 'eye',
          tooltipText: 'Voir les détails',
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
  ) {
  }

  getByType$(type: ClaimsEnum, filters): Observable<OrganizationModel[]> {
    return combineLatest([
      this.afs.collection<OrganizationModel>('organizations').valueChanges(),
      this.afs.collection<OrganizationModel>('organizationsPrivates').valueChanges(),
    ]).pipe(
      map(([data1, data2]: [OrganizationModel[], OrganizationModel[]]) => data1.map((org, index) => ({...org, ...data2[index]}))),
    );

  }

  private _manageError() {
    return pipe(
      catchError((err) => {
        console.error('--------------', err);
        return of(err);
      }),
    );
  }

}
