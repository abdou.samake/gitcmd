import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule, NzSpinModule, NzTabsModule } from 'ng-zorro-antd';
import { CruderModule } from '../../components/cruder/cruder.module';
import { LayoutsModule } from '../../components/layouts/layouts.module';

import { OrganizationsRoutingModule } from './organizations-routing.module';
import { OrganizationsComponent } from './organizations.component';


@NgModule({
  declarations: [OrganizationsComponent],
  imports: [
    CommonModule,
    OrganizationsRoutingModule,
    NzSpinModule,
    LayoutsModule,
    NzButtonModule,
    NzTabsModule,
    CruderModule,
  ],
})
export class OrganizationsModule {
}
