import { Component, OnInit } from '@angular/core';
import { OrganizationsService } from '../organizations.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.scss'],
})
export class OrganizationsComponent implements OnInit {

  isLoading = false;

  constructor(
    public organizationsService: OrganizationsService,
  ) {
  }

  ngOnInit(): void {
  }

}
