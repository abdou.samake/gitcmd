import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NzAutocompleteModule,
  NzButtonModule,
  NzDividerModule,
  NzFormModule,
  NzGridModule,
  NzInputModule,
  NzMessageServiceModule,
  NzSpinModule,
  NzSwitchModule,
} from 'ng-zorro-antd';
import { CruderModule } from '../../../components/cruder/cruder.module';
import { LayoutsModule } from '../../../components/layouts/layouts.module';

import { OrganizationRoutingModule } from './organization-routing.module';
import { OrganizationComponent } from './organization.component';


@NgModule({
  declarations: [OrganizationComponent],
  imports: [
    CommonModule,
    OrganizationRoutingModule,
    NzSpinModule,
    LayoutsModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzGridModule,
    NzFormModule,
    NzInputModule,
    NzSwitchModule,
    NzMessageServiceModule,
    FormsModule,
    NzAutocompleteModule,
    NzDividerModule,
    CruderModule,
  ],
})
export class OrganizationModule {
}
