import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ClaimType } from '../../../models/claims.model';
import { OrganizationModel } from '../../../models/organization.model';
import { UserModel, UserRolesEnum } from '../../../models/user.model';
import { OrganizationService } from '../../../services/organization.service';
import { SearchService } from '../../../services/search.service';
import { OrganizationUsersService } from './organization-users.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss'],
  providers: [OrganizationUsersService],
})
export class OrganizationComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();

  userSearchForm: FormGroup;
  managerSearchForm: FormGroup;
  organizationForm: FormGroup;

  organization: OrganizationModel;

  isLoading = false;
  usersList: UserModel[] = [];
  managersList: UserModel[] = [];

  constructor(
    private fb: FormBuilder,
    private organizationService: OrganizationService,
    private nzMessageService: NzMessageService,
    private route: ActivatedRoute,
    private searchService: SearchService,
    public organizationUsersService: OrganizationUsersService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.userSearchForm = this.fb.group({
      value: ['', [Validators.required]],
      user: [null, [Validators.required]],
    });
    this.managerSearchForm = this.fb.group({
      value: ['', [Validators.required]],
      user: [null, [Validators.required]],
    });
    this._initForm();

    combineLatest([
        this.userSearchForm.valueChanges
          .pipe(
            takeUntil(this.destroy$),
            map(x => x.value),
            filter(x => x?.length > 2),
            switchMap((searchString) => this.searchService.searchUser$(searchString, UserRolesEnum.isUser)),
            tap((x) => this.usersList = x),
          ),
        this.managerSearchForm.valueChanges
          .pipe(
            takeUntil(this.destroy$),
            map(x => x.value),
            filter(x => x?.length > 2),
            switchMap((searchString) => this.searchService.searchUser$(searchString, UserRolesEnum.isManager)),
            tap((x) => this.managersList = x),
          ),
      ],
    )
      .subscribe();

    this.route.params
      .pipe(
        takeUntil(this.destroy$),
        map(params => params.uid),
        filter(uid => !!uid),
        switchMap(uid => this.organizationService.getOrganisationById$(uid)),
        tap((organization) => this.organization = organization),
        tap((organization) => this._initForm(this.organization)),
        tap((organization) => this.organizationUsersService.initOrganization(this.organization)),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  addUserByType(type: ClaimType) {
    this.userSearchForm.get('user').setValue(this.userSearchForm.value.value);
    this.managerSearchForm.get('user').setValue(this.managerSearchForm.value.value);
    const userToAdd = type === 'isUser' ? {...this.userSearchForm.value.user} : {...this.managerSearchForm.value.user};
    if (userToAdd.uid) {
      this.organizationService.addUserByType$(userToAdd, this.organization, type)
        .pipe(tap((x) => this._resetSearch()))
        .subscribe();
    }
  }

  createOrganization() {
    this.organization = {...this.organization, ...this.organizationForm.value};
    this.organizationService.createOrganization$(this.organization)
      .pipe(
        tap((x) => this.nzMessageService.success('Organisation créée', {nzDuration: 2000})),
        tap((orgCreated: OrganizationModel) => this.router.navigate(['/admin', 'organizations', orgCreated.uid])),
      )
      .subscribe();
  }

  private _initForm(organization?: OrganizationModel) {
    this.organizationForm = this.fb.group({
      name: [organization?.name ?? null, [Validators.required]],
      autoValidation: [organization?.autoValidation ?? false],
    });
  }

  private _resetSearch() {
    this.userSearchForm.reset();
    this.managerSearchForm.reset();
    this.usersList = [];
    this.managersList = [];
  }

}
