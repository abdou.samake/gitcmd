import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, from, Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../../models/claims.model';
import { OrganizationModel } from '../../../models/organization.model';
import { UserLink, UserModel } from '../../../models/user.model';
import { OrganizationService } from '../../../services/organization.service';

@Injectable()
export class OrganizationUsersService implements CruderServiceModel<OrganizationModel, ClaimsEnum> {

  organization: OrganizationModel;

  keys: FilterKey[] = [
    {
      databaseKey: 'lastName',
      columnName: 'nom',
    },
    {
      databaseKey: 'firstName',
      columnName: 'Prénom',
    },
    {
      databaseKey: 'birthday',
      columnName: 'Date de naissance',
    },
    {
      databaseKey: 'zip',
      columnName: 'Prénom',
    },
    {
      databaseKey: 'gender',
      nzFilterFn: true,
      columnName: 'genre',
    },
    {
      databaseKey: 'email',
      columnName: 'email',
    },
    {
      databaseKey: 'phone',
      columnName: 'Téléphone',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'danger',
          action: (user: UserModel) => {
            this.organizationService.removeUserFromOrganization$(user, this.organization, 'isUser')
              .pipe(
                tap((x) => this.nzMessageService.success('Utilisateur bien supprimé', {nzDuration: 2000})),
              )
              .subscribe();
          },
          iconName: 'delete',
          tooltipText: `Supprimer de l'organisation`,
          condition: () => true,
        },
        {
          type: 'default',
          action: (user: UserModel) => {
            return this.router.navigate(['/admin', 'users', user.uid]);
          },
          iconName: 'eye',
          tooltipText: `Détail`,
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private organizationService: OrganizationService,
    private nzMessageService: NzMessageService,
    private router: Router,
  ) {
  }

  getByType$(type: ClaimsEnum, filters, organization?: OrganizationModel): Observable<UserModel[]> {
    return this.afs
      .collection('organizationsPrivates').doc(organization.uid)
      .collection<UserLink>(type, ref => ref.where(type, '==', true))
      .valueChanges()
      .pipe(
        switchMap((links: UserLink[]) => links.length > 0 ? combineLatest(
          links.map(link => from(this.afs.collection('users').doc<UserModel>(link.userUid).valueChanges())),
          ) : of([]),
        ));
  }


  initOrganization(organization: OrganizationModel) {
    this.organization = organization;
  }
}
