import { TestBed } from '@angular/core/testing';

import { OrganizationUsersService } from './organization-users.service';

describe('OrganizationUsersService', () => {
  let service: OrganizationUsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrganizationUsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
