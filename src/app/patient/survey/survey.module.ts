import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzAlertModule } from 'ng-zorro-antd';
import { LayoutsModule } from '../../components/layouts/layouts.module';
import { SatisfactionSurveyModule } from '../../components/satisfaction-survey/satisfaction-survey.module';

import { SurveyRoutingModule } from './survey-routing.module';
import { SurveyComponent } from './survey.component';


@NgModule({
  declarations: [SurveyComponent],
  imports: [
    CommonModule,
    SurveyRoutingModule,
    LayoutsModule,
    SatisfactionSurveyModule,
    NzAlertModule,
  ],
})
export class SurveyModule { }
