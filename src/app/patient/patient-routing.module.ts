import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PatientComponent } from './patient.component';

const routes: Routes = [
  {
    path: '', component: PatientComponent,
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
  },
  {
    path: 'events-list',
    loadChildren: () => import('./events-list/events-list.module').then(m => m.EventsListModule),
  },
  {
    path: ':userUid/survey/:eventUid',
    loadChildren: () => import('./survey/survey.module').then(m => m.SurveyModule),
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientRoutingModule {
}
