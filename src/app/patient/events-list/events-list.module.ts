import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    NzButtonModule,
    NzCardModule,
    NzDividerModule,
    NzGridModule,
    NzListModule, NzSpinModule,
    NzTypographyModule,
} from 'ng-zorro-antd';
import { LayoutsModule } from '../../components/layouts/layouts.module';
import { SeatsDisplayerModule } from '../../components/seats-displayer/seats-displayer.module';
import { PipesModule } from '../../pipes/pipes.module';

import { EventsListRoutingModule } from './events-list-routing.module';
import { EventsListComponent } from './events-list.component';


@NgModule({
  declarations: [EventsListComponent],
    imports: [
        CommonModule,
        EventsListRoutingModule,
        NzGridModule,
        NzCardModule,
        LayoutsModule,
        NzTypographyModule,
        NzDividerModule,
        NzListModule,
        NzButtonModule,
        PipesModule,
        NzSpinModule,
        SeatsDisplayerModule,
    ],
})
export class EventsListModule {
}
