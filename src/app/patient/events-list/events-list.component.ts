import { Component, OnDestroy, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { first, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ClaimsEnum } from '../../models/claims.model';
import { FullEventModel } from '../../models/eventModel';
import { UserModel } from '../../models/user.model';
import { EventService } from '../../services/event.service';
import { EventsService } from '../../services/events.service';
import { UserService } from '../../services/user.service';
import { WorkshopsService } from '../../services/workshops.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss'],
  providers: [EventsService],
})
export class EventsListComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  fullEvents: FullEventModel[];
  openedFullEvents: FullEventModel[];
  user: UserModel;
  loading = true;

  constructor(
    private workshopsService: WorkshopsService,
    private eventsService: EventsService,
    private userService: UserService,
    private eventService: EventService,
    private nzMessageService: NzMessageService,
  ) {
  }

  ngOnInit(): void {
    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        first(),
        tap((user: UserModel) => this.user = user),
        switchMap((user: UserModel) => this.eventsService.getAllMyEvents$(user)),
        tap((events) => this.fullEvents = events),
        switchMap((user) => this.eventsService.getAllOpenEvents$(this.user, this.fullEvents)),
        tap((events) => this.openedFullEvents = events),
        tap((x) => this.loading = false),
      )
      .subscribe();
  }

  registerToAnEvent(fullEvent: FullEventModel) {
    this.loading = true;
    this.eventService.addUserToAnEventByType$(fullEvent.event, this.user, ClaimsEnum.isUser)
      .pipe(
        tap((x) => this.nzMessageService.success('Inscrit avec succès', {nzDuration: 2000})),
        tap((x) => this.loading = false),
      )
      .subscribe();
  }

  removeFromAnEvent(fullEvent: FullEventModel) {
    this.loading = true;
    this.eventService.removeUserFromAnEventByType$(fullEvent.event, this.user, ClaimsEnum.isUser)
      .pipe(
        tap((x) => this.nzMessageService.success('quitté avec succès', {nzDuration: 2000})),
        tap((x) => this.loading = false),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
