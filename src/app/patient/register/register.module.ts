import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsModule } from '../../components/layouts/layouts.module';
import { RegisterListModule } from '../../components/register-list/register-list.module';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';


@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    RegisterListModule,
    LayoutsModule,
  ],
})
export class RegisterModule { }
