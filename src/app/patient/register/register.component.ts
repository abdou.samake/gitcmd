import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {

  user: UserModel;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.userService.getUser$()
      .pipe(
        tap((user) => this.user = user),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
