import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzMessageModule } from 'ng-zorro-antd';
import {LayoutsModule} from '../components/layouts/layouts.module';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientComponent } from './patient.component';


@NgModule({
  declarations: [PatientComponent],
    imports: [
        CommonModule,
        PatientRoutingModule,
        NzMessageModule,
        LayoutsModule,
    ]
})
export class PatientModule { }
