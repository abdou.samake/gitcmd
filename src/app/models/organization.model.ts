import { DocumentReference } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import FieldValue = firebase.firestore.FieldValue;
import Timestamp = firebase.firestore.Timestamp;

export interface OrganizationModel {
  uid?: string;
  ref?: DocumentReference;
  name?: string;
  autoValidation?: boolean;
  creationDate?: Timestamp | FieldValue;
}

export interface OrganizationLink {
  organizationRef: DocumentReference;
  organizationUid: string;
}
