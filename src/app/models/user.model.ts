import DocumentReference = firebase.firestore.DocumentReference;
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase/app';
import { PatientValidationState } from './patient-validation-state.model';

export interface UserModel {
  uid?: string;
  ref?: DocumentReference;

  email?: string;
  userName?: string;
  firstName?: string;
  lastName?: string;
  gender?: string;

  zip?: string;
  phone?: string;
  birthday?: any;
  photoURL?: string;
  creationDate?: Timestamp;
  displayName?: string;
  phoneNumber?: string;
  disabled?: boolean;

  notificationByMail?: boolean;

  emailVerified?: boolean;
  validationState?: PatientValidationState;

  isUser?: boolean;
  isAdmin?: boolean;
  isManager?: boolean;
  isPds?: boolean;
}

export interface UserLink {
  userRef: DocumentReference;
  userUid: string;
  isUser?: boolean;
  isManager?: boolean;
}


export enum UserRolesEnum {
  isUser = 'isUser',
  isAdmin = 'isAdmin',
  isManager = 'isManager',
  isPds = 'isPds'
}
