export interface ClaimsModel {
    isAdmin: boolean;
    isManager: boolean;
    isUser: boolean;
    isPds: boolean;
    uid: string;
}

export enum ClaimsEnum {
    isAdmin = 'isAdmin',
    isManager = 'isManager',
    isUser = 'isUser',
    isPds = 'isPds',
}

export enum ClaimsRoleNameInDB {
  isAdmin = 'admins',
  isManager = 'managers',
  isUser = 'users',
  isPds = 'pds',
}

export type ClaimType = 'isAdmin' | 'isManager' | 'isUser' | 'isPds';
export type ClaimDatabaseType = 'admins' | 'managers' | 'users' | 'pds';
