import { EventModel } from './eventModel';
import { OrganizationModel } from './organization.model';
import { SurveyModel } from './survey.model';
import { UserModel } from './user.model';
import { WorkshopModel } from './workshop.model';

export interface MonitoringModel extends UserModel{
  organization?: OrganizationModel;
  event?: EventModel;
  workshop?: WorkshopModel;
  userStatusInEvent?: UserMonitoringStatus;
  wasLate?: boolean;
  survey?: SurveyModel;
  comment?: string;
  organizers?: UserModel[];
}

export enum UserMonitoringStatus {
  present= 'present',
  absent = 'absent',
  excused = 'excused',
}
