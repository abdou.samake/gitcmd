export interface SurveyModel {
  participated: boolean;
  difficulties: boolean;
  videoQuality: number;
  soundQuality: number;
  usability: number;
  referral: number;
}
