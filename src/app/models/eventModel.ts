import { DocumentReference } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { OrganizationModel } from './organization.model';
import { UserModel } from './user.model';
import { WorkshopModel } from './workshop.model';
import Timestamp = firebase.firestore.Timestamp;

export class EventModel {
  uid?: string;
  ref?: DocumentReference;
  creatorUid?: string;
  creatorRef?: DocumentReference;
  creationDate?: Timestamp;
  startDate?: Timestamp;
  endDate?: Timestamp;
  published?: boolean;
  registeringClosed?: boolean;
  openToAll?: boolean;

  place?: string;
  link?: string;
  camLink?: string;

  maxSizedActivated?: boolean;
  maxSize?: number;
  reserved?: number;
  available?: number;

  state?: EventStateEnum;
  guests?: UserOfEvent[];
  workshopUid?: string;
  workshopRef?: DocumentReference;
}


export interface UserOfEvent extends UserModel {
  eventUid: string;
  eventRef: DocumentReference;
  registered: boolean;
  present: boolean;
  absent: boolean;
  connectionFailure: boolean;
  excused: boolean;
  comment: string;
}


export enum EventStateEnum {
  started = 'started',
  done = 'done',
  scheduled = 'scheduled',
}

export interface FullEventModel {
  event: EventModel;
  workshop: WorkshopModel;
  organization: OrganizationModel;
}
