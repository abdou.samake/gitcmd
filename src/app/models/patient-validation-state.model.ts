export enum PatientValidationState {
  completed = 'completed',
  toComplete = 'toComplete',
  validated = 'validated',
  error = 'error',
}

export enum PatientValidationStateText {
  completed = 'Validation en cours',
  toComplete = 'A compléter',
  validated = 'Validé',
  error = 'Erreur',
}

export enum PatientValidationStateColor {
  completed = 'gold',
  toComplete = 'orange',
  validated = 'green',
  error = 'red'
}
