import { DocumentReference } from '@angular/fire/firestore';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase/app';

export interface WorkshopModel {
  uid: string;
  ref?: DocumentReference;
  name: string;
  description?: string;
  organizationUid?: string;
  organizationRef?: DocumentReference;
  creationDate?: Timestamp;
  creatorUid?: string;
  creatorRef?: DocumentReference;
}
