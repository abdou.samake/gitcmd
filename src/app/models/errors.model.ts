export enum ErrorsModelEnum {
  is_adult= 'Vous devez être adulte',
  is_cgu_validated = 'Vous devez valider les CGU',
  email = 'Vous devez renseigner un email valide',
  password = 'Vous devez renseigner un mot de passe de plus de 6 caractères'

}
