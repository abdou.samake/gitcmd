export interface PageHeaderTagModel {
  name: string;
  color: string;
}

export interface BannerHeaderModel {
  color: string;
  title: string;
}

export interface AlertHeaderModel {
  type: 'success' | 'info' | 'warning' | 'error';
  message: string;
  description: string;
  showIcon: boolean;
}
