import { Pipe, PipeTransform } from '@angular/core';
import { UserModel, UserRolesEnum } from '../models/user.model';


@Pipe({
  name: 'userRoles',
  pure: true,
})
export class UserRolesPipe implements PipeTransform {

  transform(user: UserModel, ...args: unknown[]): string {
    const mapName: Map<string, string> = new Map<string, string>()
      .set('isAdmin', 'SuperAdministrateur')
      .set('isManager', 'Administrateur')
      .set('isPds', 'Professionnel')
      .set('isUser', 'Utilisateur')
    ;
    return Object.keys(UserRolesEnum)
      .reduce((acc, role) => acc + `${user && user[role] ? mapName.get(role) + ' |' : ''} `, '');
  }

}
