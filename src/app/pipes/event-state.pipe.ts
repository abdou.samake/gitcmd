import { Pipe, PipeTransform } from '@angular/core';
import { PageHeaderTagModel } from '../models/page-header-tag.model';

@Pipe({
  name: 'eventState',
  pure: true,
})
export class EventStatePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): PageHeaderTagModel {
    switch (value) {
      case 'started' :
        return {
          name: 'En cours',
          color: 'gold'
        };
      case 'done' :
        return {
          name: 'Terminé',
          color: 'red'
        };
      case 'scheduled' :
        return {
          name: 'À venir',
          color: 'orange'
        };
      case 'canceled' :
        return {
          name: 'Annulé',
          color: 'red'
        };

      default:
        return {
          name: '',
          color: '',
        };

    }
  }

}
