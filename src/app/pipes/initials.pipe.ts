import { Pipe, PipeTransform } from '@angular/core';
import { UserModel } from '../models/user.model';

@Pipe({
  name: 'initials',
  pure: true,
})
export class InitialsPipe implements PipeTransform {

  transform(user: UserModel, ...args: unknown[]): unknown {
    if (!user?.lastName || !user?.lastName) {
      return '';
    }
    return user.firstName.charAt(0).toUpperCase() + user.lastName.charAt(0).toUpperCase();
  }

}
