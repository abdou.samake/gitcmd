import { Pipe, PipeTransform } from '@angular/core';
import { PageHeaderTagModel } from '../models/page-header-tag.model';
import { PatientValidationStateColor, PatientValidationStateText } from '../models/patient-validation-state.model';

@Pipe({
  name: 'patientState',
  pure: true,
})
export class PatientStatePipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): PageHeaderTagModel {
    if (!value) {
      return {
        name: '',
        color: '',
      };
    }
    const name = value
      ? PatientValidationStateText[Object.keys(PatientValidationStateText).find(key => key === value)]
      : PatientValidationStateText.error;
    const color = value
      ? PatientValidationStateColor[Object.keys(PatientValidationStateColor).find(key => key === value)]
      : PatientValidationStateColor.error;
    return {name, color};
  }
}
