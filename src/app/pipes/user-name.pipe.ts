import { Pipe, PipeTransform } from '@angular/core';
import { UserModel } from '../models/user.model';

@Pipe({
  name: 'userName',
  pure: true,
})
export class UserNamePipe implements PipeTransform {

  transform(value: UserModel, ...args: unknown[]): string {
    return `${value?.firstName ?? ''} ${value?.lastName ?? ''}`;
  }

}
