import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EventStatePipe } from './event-state.pipe';
import { InitialsPipe } from './initials.pipe';
import { PatientStatePipe } from './patient-state.pipe';
import { UserNamePipe } from './user-name.pipe';
import { UserRolesPipe } from './user-roles.pipe';
import { EventFullNamePipe } from './event-full-name.pipe';

@NgModule({
  declarations: [
    PatientStatePipe,
    UserNamePipe,
    InitialsPipe,
    EventStatePipe,
    UserRolesPipe,
    EventFullNamePipe,
  ],
    exports: [
        PatientStatePipe,
        UserNamePipe,
        InitialsPipe,
        EventStatePipe,
        UserRolesPipe,
        EventFullNamePipe,
    ],
  imports: [
    CommonModule,
  ],
})
export class PipesModule {
}
