import { Pipe, PipeTransform } from '@angular/core';
import { FullEventModel } from '../models/eventModel';

@Pipe({
  name: 'eventFullName',
  pure: true,
})
export class EventFullNamePipe implements PipeTransform {

  transform(fullEvent: FullEventModel, ...args: unknown[]): string {
    if (!fullEvent?.organization?.name) {
      return '';
    }
    if (!fullEvent?.workshop?.name) {
      return '';
    }
    return ` ${fullEvent?.organization?.name} |  ${fullEvent?.workshop?.name}`;
  }

}
