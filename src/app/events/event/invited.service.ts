import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../models/claims.model';
import { EventModel } from '../../models/eventModel';
import { MonitoringModel } from '../../models/monitoring.model';
import { UserModel } from '../../models/user.model';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';

@Injectable()
export class InvitedService implements CruderServiceModel<MonitoringModel, ClaimsEnum> {

  user: UserModel;
  event: EventModel;
  keys: FilterKey[] = [
    {
      databaseKey: 'lastName',
      columnName: 'Nom',
    },
    {
      databaseKey: 'firstName',
      columnName: 'Prénom',
    },
    {
      columnName: 'Situation',
      withMonitoring: true,
      databaseKey: 'Prénom',
      condition: function () {
        return this.user?.isAdmin || this.user?.isManager;
      }.bind(this),
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'danger',
          action: (user: UserModel) => {
            this.eventService.removeUserFromAnEventByType$(this.event, user, ClaimsEnum.isUser)
              .pipe(
                tap((x) => this.nzMessageService.success('supprimé', {nzDuration: 2000})),
              )
              .subscribe();
          },
          iconName: 'delete',
          tooltipText: 'Supprimer',
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private eventService: EventService,
    private nzMessageService: NzMessageService,
    private userService: UserService,
  ) {
  }

  getByType$(type: ClaimsEnum, filters, event: EventModel): Observable<MonitoringModel[]> {
    this.event = event;
    return this.userService.getUser$()
      .pipe(
        tap((x) => this.user = x),
        switchMap(() => this.eventService.getAllUsersOfAnEventByType$(this.event, ClaimsEnum.isUser),
        ),
      );
  }
}
