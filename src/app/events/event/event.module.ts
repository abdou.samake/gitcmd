import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
    NzAutocompleteModule,
    NzButtonModule,
    NzCardModule, NzDatePickerModule, NzFormModule,
    NzGridModule,
    NzIconModule, NzInputModule, NzMessageServiceModule,
    NzSpinModule,
    NzStatisticModule, NzSwitchModule,
    NzTabsModule,
} from 'ng-zorro-antd';
import { CruderModule } from '../../components/cruder/cruder.module';
import { LayoutsModule } from '../../components/layouts/layouts.module';
import { SatisfactionSurveyModule } from '../../components/satisfaction-survey/satisfaction-survey.module';
import { PipesModule } from '../../pipes/pipes.module';

import { EventRoutingModule } from './event-routing.module';
import { EventComponent } from './event.component';


@NgModule({
  declarations: [EventComponent],
    imports: [
        CommonModule,
        EventRoutingModule,
        NzSpinModule,
        LayoutsModule,
        NzButtonModule,
        NzTabsModule,
        PipesModule,
        NzGridModule,
        NzCardModule,
        NzStatisticModule,
        NzIconModule,
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        NzSwitchModule,
        NzMessageServiceModule,
        NzAutocompleteModule,
        CruderModule,
        SatisfactionSurveyModule,
        NzDatePickerModule,
    ],
})
export class EventModule {
}
