import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, Subject } from 'rxjs';
import { filter, first, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ClaimsEnum } from '../../models/claims.model';
import { EventModel, FullEventModel } from '../../models/eventModel';
import { UserModel, UserRolesEnum } from '../../models/user.model';
import { EventService } from '../../services/event.service';
import { SearchService } from '../../services/search.service';
import { UserService } from '../../services/user.service';
import { InvitedSurveyService } from './invited-survey.service';
import { InvitedService } from './invited.service';
import { OrganizerService } from './organizer.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  providers: [
    InvitedService,
    OrganizerService,
    InvitedSurveyService,
  ],
})
export class EventComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  isLoading = true;
  fullEvent: FullEventModel;
  usersList: UserModel[] = [];
  user: UserModel;

  eventForm: FormGroup;
  searchUserForm: FormGroup;
  searchManagerForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService,
    private fb: FormBuilder,
    private nzMessageService: NzMessageService,
    private searchService: SearchService,
    private userService: UserService,
    public invitedService: InvitedService,
    public invitedSurveyService: InvitedSurveyService,
    public organizerService: OrganizerService,
  ) {
  }

  ngOnInit(): void {
    this.searchUserForm = this.fb.group({
      value: ['', [Validators.required]],
      user: [null, [Validators.required]],
    });
    this.searchManagerForm = this.fb.group({
      value: ['', [Validators.required]],
      user: [null, [Validators.required]],
    });

    this.searchUserForm.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map(x => x.value),
        filter(x => x?.length > 2),
        switchMap((searchString) => this.searchService.searchUser$(searchString, UserRolesEnum.isUser)),
        tap((x) => this.usersList = x),
      )
      .subscribe();

    this.searchManagerForm.valueChanges
      .pipe(
        takeUntil(this.destroy$),
        map(x => x.value),
        filter(x => x?.length > 2),
        switchMap((searchString) => this.searchService.searchUser$(searchString, UserRolesEnum.isManager)),
        tap((x) => this.usersList = x),
      )
      .subscribe();

    combineLatest([
      this.userService.getUser$()
        .pipe(
          first(),
          tap((x) => this.user = x)
        ),
      this.route.params,
    ])
      .pipe(
        takeUntil(this.destroy$),
        map(([a, b]) => b),
        map(params => params.uid),
        filter(uid => !!uid),
        switchMap((uid: string) => this.eventService.getFullEventById$(uid)),
        tap((event) => this.fullEvent = event),
        tap((x) => this._initEventForm(this.fullEvent.event)),
        tap((x) => this.isLoading = false),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  saveEvent() {
    this.eventService.saveEvent$({...this.fullEvent.event, ...this.eventForm.value})
      .pipe(
        tap((x) => this.nzMessageService.success('Sauvé', {nzDuration: 2000})),
      )
      .subscribe();
  }

  addUserToAnEvent() {
    this.eventService.addUserToAnEventByType$(this.fullEvent.event, this.searchUserForm.value.value, ClaimsEnum.isUser)
      .pipe(
        tap((x) => this.eventForm.reset()),
        tap((x) => this.nzMessageService.success('Ajouté', {nzDuration: 2000})),
      )
      .subscribe();
  }

  addManagerToAnEvent(user?: UserModel) {
    this.eventService.addUserToAnEventByType$(this.fullEvent.event, user ?? this.searchManagerForm.value.value, ClaimsEnum.isManager)
      .pipe(
        tap((x) => this.eventForm.reset()),
        tap((x) => this.nzMessageService.success('Ajouté', {nzDuration: 2000})),
      )
      .subscribe();
  }

  private _initEventForm(event: EventModel) {
    this.eventForm = this.fb.group({
      startDate: [event?.startDate?.toDate() ?? null, [Validators.required]],
      endDate: [event?.endDate?.toDate() ?? null, [Validators.required]],
      place: [event?.place ?? null, []],
      camLink: [event?.camLink ?? null, [Validators.required]],
      link: [event?.link ?? null],
      published: [event?.published ?? null],
      maxSizedActivated: [event?.maxSizedActivated ?? null],
      maxSize: [event?.maxSize ?? null],
      registeringClosed: [event?.registeringClosed ?? null],
      openToAll: [event?.openToAll ?? null],
    });


  }

}
