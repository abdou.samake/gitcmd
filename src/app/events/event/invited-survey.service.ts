import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../models/claims.model';
import { EventModel } from '../../models/eventModel';
import { MonitoringModel } from '../../models/monitoring.model';
import { SurveyModel } from '../../models/survey.model';
import { UserModel } from '../../models/user.model';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';

@Injectable()
export class InvitedSurveyService implements CruderServiceModel<MonitoringModel, ClaimsEnum> {

  user: UserModel;
  event: EventModel;
  keys: FilterKey[] = [
    {
      databaseKey: 'lastName',
      columnName: 'Nom',
    },
    {
      databaseKey: 'firstName',
      columnName: 'Prénom',
    },
    {
      databaseKey: 'participated',
      columnName: 'Participation',
    },
    {
      databaseKey: 'difficulties',
      columnName: 'Pb connexion',
    },
    {
      databaseKey: 'videoQuality',
      columnName: 'Qualité vidéo',
    },
    {
      databaseKey: 'soundQuality',
      columnName: 'Qualité son',
    },
    {
      databaseKey: 'usability',
      columnName: 'Utilité',
    },
    {
      databaseKey: 'referral',
      columnName: 'Recommandation',
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private eventService: EventService,
    private nzMessageService: NzMessageService,
    private userService: UserService,
  ) {
  }

  getByType$(type: ClaimsEnum, filters, event: EventModel): Observable<MonitoringModel[]> {
    this.event = event;
    return this.eventService.getAllUsersOfAnEventByType$(this.event, type)
      .pipe(
        switchMap((list: MonitoringModel[]) => list?.length > 0 ? combineLatest(
          list.map(monit => this.afs
            .collection('eventsPrivates')
            .doc(monit.event.uid)
            .collection('surveys')
            .doc<SurveyModel>(monit.uid)
            .valueChanges().pipe(map(data => ({...monit, survey: data, ...data}))))
        ) : of([])),
      );
  }

}
