import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { combineLatest, Observable, of } from 'rxjs';
import { first, switchMap, tap } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../models/claims.model';
import { EventModel } from '../../models/eventModel';
import { UserModel } from '../../models/user.model';
import { EventService } from '../../services/event.service';
import { UserService } from '../../services/user.service';

@Injectable()
export class OrganizerService implements CruderServiceModel<UserModel, ClaimsEnum> {

  isUser = true;
  event: EventModel;
  keys: FilterKey[] = [
    {
      databaseKey: 'lastName',
      columnName: 'Nom',
    },
    {
      databaseKey: 'firstName',
      columnName: 'Prénom',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      condition: () => !this.isUser,
      buttons: [
        {
          type: 'danger',
          action: (user: UserModel) => {
            this.eventService.removeUserFromAnEventByType$(this.event, user, ClaimsEnum.isManager)
              .pipe(
                tap((x) => this.nzMessageService.success('supprimé', {nzDuration: 2000})),
              )
              .subscribe();
          },
          iconName: 'delete',
          tooltipText: 'Supprimer',
          condition: () => !this.isUser,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private eventService: EventService,
    private nzMessageService: NzMessageService,
    private userService: UserService,
  ) {
  }

  getByType$(type: ClaimsEnum, filters, event: EventModel): Observable<UserModel[]> {
    this.event = event;

    return this.userService.getUser$()
      .pipe(
        first(),
        tap((x) => this.isUser = x.isUser),
        switchMap(() => this.afs
          .collection('eventsPrivates').doc(event.uid)
          .collection<{ userUid: string }>(type).valueChanges()
          .pipe(
            switchMap((users) => users.length > 0
              ? combineLatest(users.map((user) =>
                this.afs
                  .collection('users')
                  .doc<UserModel>(user.userUid)
                  .valueChanges()),
              )
              : of([])),
          )),
      );


  }
}
