import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzButtonModule, NzSpinModule, NzTabsModule } from 'ng-zorro-antd';
import { LayoutsModule } from '../components/layouts/layouts.module';

import { EventsRoutingModule } from './events-routing.module';
import { EventsComponent } from './events.component';


@NgModule({
  declarations: [EventsComponent],
  imports: [
    CommonModule,
    EventsRoutingModule,
    NzSpinModule,
    LayoutsModule,
    NzButtonModule,
    NzTabsModule,
  ],
})
export class EventsModule { }
