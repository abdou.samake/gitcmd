import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { combineLatest, Observable, of, pipe } from 'rxjs';
import { catchError, first, switchMap } from 'rxjs/operators';
import { CruderServiceModel, FilterKey } from '../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../models/claims.model';
import { OrganizationLink, OrganizationModel } from '../../models/organization.model';
import { UserService } from '../../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class MyOrganizationsService implements CruderServiceModel<OrganizationModel, ClaimsEnum> {

  keys: FilterKey[] = [
    {
      databaseKey: 'name',
      columnName: 'Nom',
    },
    {
      databaseKey: 'autoValidation',
      columnName: 'Validation auto',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'default',
          action: (organization: OrganizationModel) => {
            return this.router.navigate(['/manager/my-organizations/workshops', organization.uid]);
          },
          iconName: 'eye',
          tooltipText: 'Voir les détails',
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private userService: UserService,
  ) {
  }

  getByType$(type: ClaimsEnum, filters): Observable<OrganizationModel[]> {
    return this.userService.getUser$()
      .pipe(
        first(),
        switchMap((user) =>
          user?.isAdmin
            ? this.afs
              .collection<OrganizationModel>('organizations')
              .valueChanges()
            : this.afs
              .collection('users')
              .doc(user.uid)
              .collection<OrganizationLink>('organizations')
              .valueChanges().pipe(
              switchMap((orgsLinks: OrganizationLink[]) =>
                orgsLinks?.length > 0
                  ? combineLatest(orgsLinks.map((link) => this.afs
                    .collection('organizations')
                    .doc(link.organizationUid)
                    .valueChanges()),
                  )
                  : of([]),
              ),
              ),
        ),
      )
      ;

  }

  private _manageError() {
    return pipe(
      catchError((err) => {
        console.error('--------------', err);
        return of(err);
      }),
    );
  }

}
