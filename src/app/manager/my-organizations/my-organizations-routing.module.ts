import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MyOrganizationsComponent } from './my-organizations.component';

const routes: Routes = [
  {
    path: '', component: MyOrganizationsComponent,
  },
  {
    path: 'workshops/:uid',
    loadChildren: () => import('./my-organization/my-organization.module').then(m => m.MyOrganizationModule),
  },
  {
    path: 'events/:uid',
    loadChildren: () => import('./my-organization-events/my-organization-events.module').then(m => m.MyOrganizationEventsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyOrganizationsRoutingModule {
}
