import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyOrganizationComponent } from './my-organization.component';

const routes: Routes = [{ path: '', component: MyOrganizationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyOrganizationRoutingModule { }
