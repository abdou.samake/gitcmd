import { TestBed } from '@angular/core/testing';

import { MyOrganizationService } from './my-organization-events.service';

describe('MyOrganizationService', () => {
  let service: MyOrganizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyOrganizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
