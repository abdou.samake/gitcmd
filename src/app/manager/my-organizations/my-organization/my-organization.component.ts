import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { OrganizationUsersService } from '../../../admin/organizations/organization/organization-users.service';
import { OrganizationModel } from '../../../models/organization.model';
import { UserModel } from '../../../models/user.model';
import { WorkshopModel } from '../../../models/workshop.model';
import { EventsService } from '../../../services/events.service';
import { OrganizationService } from '../../../services/organization.service';
import { SearchService } from '../../../services/search.service';
import { UserService } from '../../../services/user.service';
import { MyOrganizationWorkshopsService } from './my-organization-workshops.service';

@Component({
  selector: 'app-my-organization',
  templateUrl: './my-organization.component.html',
  styleUrls: ['./my-organization.component.scss'],
  providers: [MyOrganizationWorkshopsService, EventsService, OrganizationUsersService],
})
export class MyOrganizationComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  isLoading = true;
  searchForm: FormGroup;
  organizationForm: FormGroup;
  workshopForm: FormGroup;
  usersList: UserModel[] = [];
  user: UserModel;

  organization: OrganizationModel;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private organizationService: OrganizationService,
    private eventsService: EventsService,
    private userService: UserService,
    private searchService: SearchService,
    public myOrganizationWorkshopsService: MyOrganizationWorkshopsService,
    public organizationUsersService: OrganizationUsersService,
  ) {
  }

  ngOnInit(): void {
    this._initForm();
    this._initWorkshopForm();
    this.searchForm.valueChanges
      .pipe(
        map(x => x.value),
        filter(x => x?.length > 2),
        switchMap((searchString) => this.searchService.searchUser$(searchString)),
        tap((x) => this.usersList = x),
      )
      .subscribe();
    combineLatest([
      this.userService.getUser$().pipe(tap((x) => this.user = x)),
      this.route.params,
    ])
      .pipe(
        map(([a, b]) => b),
        takeUntil(this.destroy$),
        map(params => params.uid),
        filter(uid => !!uid),
        switchMap((uid: string) => this.organizationService.getOrganisationById$(uid)),
        tap((organization) => this.organization = organization),
        tap(() => this._initForm(this.organization)),
        tap((x) => this.isLoading = false),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  addEvent() {
    this.eventsService.createWorkshop$(this.workshopForm.value, this.user, this.organization)
      .pipe(
        tap((x) => this.workshopForm.reset()),
      )
      .subscribe();
  }
  addUser$() {
    this.searchForm.get('user').setValue(this.searchForm.value.value);
    const userToAdd = {...this.searchForm.value.user};
    if (this.searchForm.get('value').valid && this.searchForm.get('user').valid) {
      this.organizationService.addUserByType$(userToAdd, this.organization, 'isUser')
        .pipe(tap((x) => this._resetSearch()))
        .subscribe();
    }
  }
  private _initForm(organization?: OrganizationModel) {
    this.searchForm = this.fb.group({
      value: ['', [Validators.required]],
      user: [null, [Validators.required]],
    });

    this.organizationForm = this.fb.group({
      name: [organization?.name ?? null, [Validators.required]],
      autoValidation: [organization?.autoValidation ?? false],
    });
  }
  private _initWorkshopForm(workshop?: WorkshopModel) {
    this.workshopForm = this.fb.group({
      name: [workshop?.name ?? null, [Validators.required]],
      description: [workshop?.description ?? null, [Validators.required]],
    });
  }
  private _resetSearch() {
    this.searchForm.reset();
    this.usersList = [];
  }
}
