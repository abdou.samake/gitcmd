import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { CruderServiceModel, FilterKey } from '../../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../../models/claims.model';
import { EventModel } from '../../../models/eventModel';
import { OrganizationModel } from '../../../models/organization.model';
import { WorkshopModel } from '../../../models/workshop.model';

@Injectable()
export class MyOrganizationWorkshopsService implements CruderServiceModel<EventModel, ClaimsEnum> {

  organization: OrganizationModel;

  keys: FilterKey[] = [
    {
      databaseKey: 'name',
      columnName: 'nom',
    },
    {
      databaseKey: 'description',
      columnName: 'description',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'default',
          action: (workshop: WorkshopModel) => {
            return this.router.navigate(['/manager/my-organizations/events', workshop.uid]);
          },
          iconName: 'eye',
          tooltipText: 'Voir les détails',
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
  ) {
  }

  save(event: EventModel, key: FilterKey, test) {
    from(this.afs.collection<EventModel>('workshops').doc(event.uid).set({...event, [key.databaseKey]: test}, {merge: true})).subscribe();
  }

  getByType$(type: ClaimsEnum, filters, organization?: OrganizationModel): Observable<EventModel[]> {
    return this.afs
      .collection<EventModel>('workshops', ref => ref
        .where('organizationUid', '==', organization.uid)).valueChanges();
  }

}
