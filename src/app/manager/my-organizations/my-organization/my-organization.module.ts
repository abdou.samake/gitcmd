import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {
  NzAutocompleteModule, NzButtonModule, NzCollapseModule,
  NzDividerModule,
  NzFormModule,
  NzGridModule, NzInputModule, NzMessageService, NzMessageServiceModule,
  NzSpinModule,
  NzSwitchModule,
} from 'ng-zorro-antd';
import { CruderModule } from '../../../components/cruder/cruder.module';
import { LayoutsModule } from '../../../components/layouts/layouts.module';

import { MyOrganizationRoutingModule } from './my-organization-routing.module';
import { MyOrganizationComponent } from './my-organization.component';


@NgModule({
  declarations: [MyOrganizationComponent],
  imports: [
    CommonModule,
    MyOrganizationRoutingModule,
    NzSpinModule,
    LayoutsModule,
    ReactiveFormsModule,
    NzGridModule,
    NzFormModule,
    NzSwitchModule,
    NzAutocompleteModule,
    CruderModule,
    NzDividerModule,
    NzButtonModule,
    NzInputModule,
    NzMessageServiceModule,
    NzCollapseModule,
  ],
})
export class MyOrganizationModule { }
