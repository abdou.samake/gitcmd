import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { combineLatest, from, Observable } from 'rxjs';
import { CruderServiceModel, FilterKey } from '../../../components/cruder/cruderServiceModel';
import { ClaimsEnum } from '../../../models/claims.model';
import { EventModel } from '../../../models/eventModel';
import { OrganizationModel } from '../../../models/organization.model';
import { WorkshopModel } from '../../../models/workshop.model';

@Injectable()
export class MyOrganizationEventsService implements CruderServiceModel<EventModel, ClaimsEnum> {

  organization: OrganizationModel;

  keys: FilterKey[] = [
    {
      databaseKey: 'link',
      columnName: 'Lien',
    },
    {
      databaseKey: 'camLink',
      columnName: 'Id Réunion',
    },
    {
      databaseKey: 'startDate',
      columnName: 'Début',
      dataType: 'dateWithHours',
    },
    {
      databaseKey: 'endDate',
      columnName: 'fin',
      dataType: 'dateWithHours',
    },
    {
      columnName: 'Actions',
      databaseKey: 'actions',
      withButtons: true,
      buttons: [
        {
          type: 'default',
          action: (event: EventModel) => {
            return this.router.navigate(['/events', event.uid]);
          },
          iconName: 'eye',
          tooltipText: 'Voir les détails',
          condition: () => true,
        },
        {
          type: 'danger',
          action: function(event: EventModel) {
            return combineLatest([
              from(this.afs.collection('events').doc(event.uid).delete()),
              from(this.afs.collection('eventsPrivates').doc(event.uid).delete()),
            ]);
          }.bind(this),
          iconName: 'delete',
          tooltipText: 'Supprimer',
          condition: () => true,
        },
      ],
    },
  ];

  constructor(
    private afs: AngularFirestore,
    private router: Router,
  ) {
  }

  getByType$(type: ClaimsEnum, filters, workshop?: WorkshopModel): Observable<EventModel[]> {
    return this.afs
      .collection<EventModel>('events', ref => ref
        .where('workshopUid', '==', workshop.uid),
      )
      .valueChanges();
  }

}
