import { TestBed } from '@angular/core/testing';

import { MyOrganizationsService } from './my-organizations.service';

describe('MyOrganizationsService', () => {
  let service: MyOrganizationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyOrganizationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
