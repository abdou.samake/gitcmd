import { Component, OnDestroy, OnInit } from '@angular/core';
import { DocumentReference } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { OrganizationModel } from '../../../models/organization.model';
import { UserModel } from '../../../models/user.model';
import { WorkshopModel } from '../../../models/workshop.model';
import { EventsService } from '../../../services/events.service';
import { OrganizationService } from '../../../services/organization.service';
import { UserService } from '../../../services/user.service';
import { MyOrganizationEventsService } from '../my-organization/my-organization-events.service';

@Component({
  selector: 'app-my-organization-events',
  templateUrl: './my-organization-events.component.html',
  styleUrls: ['./my-organization-events.component.scss'],
  providers: [MyOrganizationEventsService, EventsService],
})
export class MyOrganizationEventsComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  isLoading = false;

  eventForm: FormGroup;

  workshop: WorkshopModel;
  organization: OrganizationModel;

  constructor(
    private route: ActivatedRoute,
    public myOrganizationEventsService: MyOrganizationEventsService,
    private organizationService: OrganizationService,
    private eventsService: EventsService,
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this._initEventForm();
    this.route.params
      .pipe(
        takeUntil(this.destroy$),
        map(params => params.uid),
        filter(uid => !!uid),
        switchMap((uid: string) => this.eventsService.getWorkshopById$(uid)),
        tap((workshop: WorkshopModel) => this.workshop = workshop),
        switchMap(() => this.organizationService.getOrganisationById$(this.workshop.organizationUid)),
        tap((organization: OrganizationModel) => this.organization = organization),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  addEvent() {
    this.userService.getUser$()
      .pipe(
        switchMap((user: UserModel) => this.eventsService.createEvent$(this.eventForm.value, user, this.organization, this.workshop)),
        tap((x) => this.eventForm.reset()),
        tap(([ref]: [DocumentReference, any]) => this.router.navigate(['/events/', ref.id])),
      )
      .subscribe();
  }

  private _initEventForm() {
    this.eventForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      place: [null],
      camLink: [null, [Validators.required]],
      link: [null, [Validators.required]],
      published: [false],
      maxSizedActivated: [false],
      maxSize: [0],
      registeringClosed: [false],
      openToAll: [false],
    });
  }

}
