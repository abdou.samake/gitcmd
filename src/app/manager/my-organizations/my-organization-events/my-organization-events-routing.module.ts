import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyOrganizationEventsComponent } from './my-organization-events.component';

const routes: Routes = [{ path: '', component: MyOrganizationEventsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyOrganizationEventsRoutingModule { }
