import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyOrganizationEventsComponent } from './my-organization-events.component';

describe('MyOrganizationEventsComponent', () => {
  let component: MyOrganizationEventsComponent;
  let fixture: ComponentFixture<MyOrganizationEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyOrganizationEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyOrganizationEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
