import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {
    NzButtonModule,
    NzCollapseModule, NzDatePickerModule,
    NzDividerModule, NzFormModule,
    NzGridModule, NzInputModule,
    NzMessageServiceModule,
    NzSpinModule,
    NzSwitchModule,
} from 'ng-zorro-antd';
import { CruderModule } from '../../../components/cruder/cruder.module';
import { LayoutsModule } from '../../../components/layouts/layouts.module';

import { MyOrganizationEventsRoutingModule } from './my-organization-events-routing.module';
import { MyOrganizationEventsComponent } from './my-organization-events.component';


@NgModule({
  declarations: [MyOrganizationEventsComponent],
    imports: [
        CommonModule,
        MyOrganizationEventsRoutingModule,
        NzSpinModule,
        LayoutsModule,
        NzDividerModule,
        NzGridModule,
        CruderModule,
        NzMessageServiceModule,
        NzCollapseModule,
        ReactiveFormsModule,
        NzSwitchModule,
        NzFormModule,
        NzInputModule,
        NzButtonModule,
        NzDatePickerModule,
    ],
})
export class MyOrganizationEventsModule { }
