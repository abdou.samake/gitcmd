import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { UserModel } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { MyOrganizationsService } from './my-organizations.service';

@Component({
  selector: 'app-my-organizations',
  templateUrl: './my-organizations.component.html',
  styleUrls: ['./my-organizations.component.scss'],
})
export class MyOrganizationsComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  isLoading = true;
  user: UserModel;

  constructor(
    public myOrganizationsService: MyOrganizationsService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        tap((x) => this.user = x),
        tap((x) => this.isLoading = false),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
