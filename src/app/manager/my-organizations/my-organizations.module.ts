import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzButtonModule, NzSpinModule, NzTabsModule } from 'ng-zorro-antd';
import { CruderModule } from '../../components/cruder/cruder.module';
import { LayoutsModule } from '../../components/layouts/layouts.module';

import { MyOrganizationsRoutingModule } from './my-organizations-routing.module';
import { MyOrganizationsComponent } from './my-organizations.component';


@NgModule({
  declarations: [MyOrganizationsComponent],
  imports: [
    CommonModule,
    MyOrganizationsRoutingModule,
    NzSpinModule,
    LayoutsModule,
    NzButtonModule,
    NzTabsModule,
    CruderModule,
  ],
})
export class MyOrganizationsModule { }
