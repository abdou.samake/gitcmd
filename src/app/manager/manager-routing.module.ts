import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManagerComponent } from './manager.component';

const routes: Routes = [
  {
    path: '',
    component: ManagerComponent,
  }, {
    path: 'my-organizations',
    loadChildren: () => import('./my-organizations/my-organizations.module').then(m => m.MyOrganizationsModule),
  },
  {
    path: 'manager-events',
    loadChildren: () => import('./manager-events/manager-events.module').then(m => m.ManagerEventsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagerRoutingModule {
}
