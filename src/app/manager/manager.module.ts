import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NzIconModule, NzLayoutModule, NzMenuModule} from "ng-zorro-antd";
import {LayoutsModule} from '../components/layouts/layouts.module';

import { ManagerRoutingModule } from './manager-routing.module';
import { ManagerComponent } from './manager.component';


@NgModule({
  declarations: [ManagerComponent],
    imports: [
        CommonModule,
        ManagerRoutingModule,
        LayoutsModule
    ]
})
export class ManagerModule { }
