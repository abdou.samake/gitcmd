import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { FullEventModel } from '../../models/eventModel';
import { UserModel } from '../../models/user.model';
import { EventsService } from '../../services/events.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-manager-events',
  templateUrl: './manager-events.component.html',
  styleUrls: ['./manager-events.component.scss'],
  providers: [EventsService],
})
export class ManagerEventsComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject<boolean>();
  loading = false;
  user: UserModel;

  fullEvents: FullEventModel[];

  constructor(
    private userService: UserService,
    private eventsService: EventsService,
  ) {
  }

  ngOnInit(): void {
    this.userService.getUser$()
      .pipe(
        takeUntil(this.destroy$),
        tap((user: UserModel) => this.user = user),
        switchMap((user: UserModel) => this.eventsService.getAllEventsOfAManager$(this.user)),
        tap((events) => this.fullEvents = events),
      )
      .subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
