import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEventsComponent } from './manager-events.component';

describe('ManagerEventsComponent', () => {
  let component: ManagerEventsComponent;
  let fixture: ComponentFixture<ManagerEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
