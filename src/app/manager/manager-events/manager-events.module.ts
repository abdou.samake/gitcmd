import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NzButtonModule,
  NzCardModule, NzDividerModule,
  NzGridModule,
  NzListModule, NzMessageServiceModule,
  NzSpinModule,
  NzTypographyModule,
} from 'ng-zorro-antd';
import { LayoutsModule } from '../../components/layouts/layouts.module';
import { SeatsDisplayerModule } from '../../components/seats-displayer/seats-displayer.module';
import { PipesModule } from '../../pipes/pipes.module';

import { ManagerEventsRoutingModule } from './manager-events-routing.module';
import { ManagerEventsComponent } from './manager-events.component';


@NgModule({
  declarations: [ManagerEventsComponent],
    imports: [
        CommonModule,
        ManagerEventsRoutingModule,
        NzSpinModule,
        LayoutsModule,
        NzListModule,
        NzCardModule,
        NzButtonModule,
        NzTypographyModule,
        NzGridModule,
        PipesModule,
        NzMessageServiceModule,
        NzDividerModule,
        SeatsDisplayerModule,
    ],
})
export class ManagerEventsModule { }
